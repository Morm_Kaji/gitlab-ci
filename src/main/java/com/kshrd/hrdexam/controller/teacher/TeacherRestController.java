package com.kshrd.hrdexam.controller.teacher;


import com.kshrd.hrdexam.controller.student.StudentRestController;
import com.kshrd.hrdexam.model.*;
import com.kshrd.hrdexam.payload.dto.studentDto.StudentLoginDetail;
import com.kshrd.hrdexam.payload.dto.studentDto.StudentResponse;
import com.kshrd.hrdexam.payload.dto.teacherDto.ChangePasswordRequest;
import com.kshrd.hrdexam.payload.dto.teacherDto.GenerationDto;
import com.kshrd.hrdexam.payload.dto.teacherDto.TeacherDto;
import com.kshrd.hrdexam.payload.dto.teacherDto.TeacherResponse;
import com.kshrd.hrdexam.payload.request.teacherRequest.ProfileUpdateRequest;
import com.kshrd.hrdexam.payload.request.teacherRequest.ResultResponse;
import com.kshrd.hrdexam.payload.request.teacherRequest.TeacherLoginRequest;
import com.kshrd.hrdexam.payload.request.teacherRequest.UpdateStudentRequest;
import com.kshrd.hrdexam.security.jwt.JwtUtil;
import com.kshrd.hrdexam.service.studentService.StudentServiceImp;
import com.kshrd.hrdexam.service.teacherService.ExamServiceImp;
import com.kshrd.hrdexam.service.teacherService.TeacherServiceImp;
import com.kshrd.hrdexam.service.userDetailService.MyUserDetailService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/teacher")
public class TeacherRestController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private MyUserDetailService userDetailService;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private TeacherServiceImp teacherServiceImp;


    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ExamServiceImp examServiceImp;

    @Autowired
    private StudentServiceImp studentServiceImp;


    public static String getCurrentUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        String username;

        if (principal instanceof UserDetails) {
            username = ((UserDetails) principal).getUsername();
        } else {
            username = principal.toString();
        }

        return username;
    }

    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @Operation(summary = "Get all student request")
    @GetMapping("/getAllStudentsRequest")
    public ResponseEntity<?> getAllStudentRequest() {

        List<StudentResponse> responseList = new ArrayList<>();

        List<StudentRequest> studentRequestList = teacherServiceImp.getAllStuRequest();

        Map<String, Object> response = new HashMap<>();

        if (!studentRequestList.isEmpty()) {

            int generationId = teacherServiceImp.getCurrentGenId();
            Generation generation = teacherServiceImp.getGenerationNameById(generationId);

            for (int i = 0; i < studentRequestList.size(); i++) {

                responseList.add(new StudentResponse(
                        studentRequestList.get(i).getId(),
                        studentRequestList.get(i).getFullName(),
                        studentRequestList.get(i).getGender(),
                        studentRequestList.get(i).getEmail(),
                        generation.getGeneration(),
                        "default.npg",
                        studentRequestList.get(i).getClassroom().getClassName(),
                        studentRequestList.get(i).getRequestAt()
                ));
            }

            response.put("status", "OK");
            response.put("message", "Get all student request success!");
            response.put("success", true);
            response.put("payload", responseList);
        } else {
            response.put("status", "OK");
            response.put("message", "No student request");
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }


    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @Operation(summary = "acceptStudentRequestById")
    @GetMapping("/acceptStudentRequestById/{id}")
    public ResponseEntity<?> acceptStudentRequestById(@PathVariable String id) {

        boolean checkId = false;

        Map<String, Object> response = new HashMap<>();

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
        Date date = new Date();

        if (StudentRestController.validation("ID", id)) {

            List<String> studentRequestList = studentServiceImp.getAllStudentRequestId();

            for (int i = 0; i < studentRequestList.size(); i++) {
                if (id.equals(studentRequestList.get(i))) {
                    checkId = true;
                }
            }

            if (checkId) {
                StudentRequest studentRequest = teacherServiceImp.getReqStuById(id);
                int generationId = teacherServiceImp.getCurrentGenId();
                Generation generation = teacherServiceImp.getGenerationNameById(generationId);


                Student student = new Student();
                StudentResponse studentResponse = new StudentResponse();

                if (studentRequest != null) {
                    student.setId(studentRequest.getId());
                    student.setFullName(studentRequest.getFullName());
                    student.setGender(studentRequest.getGender());
                    student.setEmail(studentRequest.getEmail());
                    student.setPassword(studentRequest.getPassword());
                    student.setGeneration(generation);
                    student.setProfileImg("default.png");
                    student.setClassroom(studentRequest.getClassroom());
                    student.setCreateAt(formatter.format(date));

                    studentResponse.setId(studentRequest.getId());
                    studentResponse.setFullName(studentRequest.getFullName());
                    studentResponse.setClassroom(studentRequest.getClassroom().getClassName());
                    studentResponse.setEmail(studentRequest.getEmail());
                    studentResponse.setGender(studentRequest.getGender());
                    studentResponse.setGeneration(generation.getGeneration());
                    studentResponse.setEmail(studentRequest.getEmail());
                    studentResponse.setProfileImg("default.png");
                    studentResponse.setCreateAt(formatter.format(date));

                } else {
                    response.put("status", "ERROR");
                    response.put("message", "Accept student request fail!");
                    response.put("success", false);
                    response.put("payload", null);
                }


                if (teacherServiceImp.insertAcceptedStu(student) && teacherServiceImp.deleteAcceptedStuById(student.getId())) {
                    response.put("status", "OK");
                    response.put("message", "Accept student request success!");
                    response.put("success", true);
                    response.put("payload", studentResponse);
                } else {
                    response.put("status", "ERROR");
                    response.put("message", "Accept student request fail!");
                    response.put("success", false);
                    response.put("payload", null);
                }
            } else {
                response.put("status", "ERROR");
                response.put("message", "Wrong ID can not accept!");
                response.put("success", false);
                response.put("payload", null);
            }
        } else {
            response.put("status", "ERROR");
            response.put("message", "ID must be at least 6 character!");
            response.put("success", false);
            response.put("payload", null);
        }

        return ResponseEntity.ok().body(response);
    }


    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @Operation(summary = "Login to get token")
    @PostMapping("/login")
    public ResponseEntity<?> loginTeacher(@RequestBody TeacherLoginRequest loginRequest) throws Exception {

        Map<String, Object> response = new HashMap<>();

        if (StudentRestController.validation("USERNAME", loginRequest.getUsername())) {
            if (StudentRestController.validation("PASSWORD", loginRequest.getPassword())) {

                boolean isPasswordMatch = false;

                Teacher teacher = teacherServiceImp.loadTeacherByUsername(loginRequest.getUsername());
                if (teacher != null) {
                    isPasswordMatch = passwordEncoder.matches(loginRequest.getPassword(), teacher.getPassword());

                    if (isPasswordMatch) {
                        try {
                            authenticationManager.authenticate(
                                    new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
                        } catch (BadCredentialsException e) {
                            throw new Exception("Incorrect username password!", e);
                        }

                        final UserDetails userDetails = userDetailService
                                .loadUserByUsername(loginRequest.getUsername());
                        final String jwt = jwtUtil.generateToken(userDetails);

                        TeacherDto teacherDto = new TeacherDto();

                        teacherDto.setUsername(teacher.getUsername());
                        teacherDto.setFullName(teacher.getFullName());
                        teacherDto.setEmail(teacher.getEmail());
                        teacherDto.setGender(teacher.getGender());
                        teacherDto.setToken(jwt);

                        response.put("status", "OK");
                        response.put("message", "Teacher has login successfully");
                        response.put("success", true);
                        response.put("payload", teacherDto);
                    } else {
                        response.put("status", "password");
                        response.put("message", "Incorrect password!");
                        response.put("success", false);
                        response.put("payload", null);
                    }

                } else {
                    response.put("status", "username");
                    response.put("message", "Incorrect username!");
                    response.put("success", false);
                    response.put("payload", null);
                }


            } else {
                response.put("status", "ERROR");
                response.put("message", "Password must be at least 6 character!");
                response.put("success", false);
                response.put("payload", null);
            }
        } else {
            response.put("status", "ERROR");
            response.put("message", "Username must be only character and don't have space!");
            response.put("success", false);
            response.put("payload", null);
        }


        return ResponseEntity.ok(response);
    }


    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @Operation(summary = "getAllStudentsByClassId")
    @GetMapping("/getAllStudentsByClassId/{id}")
    public ResponseEntity<?> getAllStudentsByGeneration(@PathVariable int id) {

        Map<String, Object> response = new HashMap<>();

        List<StudentResponse> studentResponseList = teacherServiceImp.getAllStudentsByClassId(id);

        if (!studentResponseList.isEmpty()) {
            response.put("status", "OK");
            response.put("message", "Get all student success!");
            response.put("success", true);
            response.put("payload", studentResponseList);
        } else {
            response.put("status", "OK");
            response.put("message", "No student in this class!");
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }


    @GetMapping("/getAllStudentsByGeneration/{id}")
    public ResponseEntity<?> getAllBasicStudentsByGeneration(@PathVariable int id) {

        GenerationDto generation1 = teacherServiceImp.getAllBasicStudentsByGeneration(id);

        Map<String, Object> response = new HashMap<>();

        if (generation1 != null && !generation1.getStudents().isEmpty()) {
            response.put("status", "OK");
            response.put("message", "Get all basic students successfully!");
            response.put("success", true);
            response.put("payload", generation1.getStudents());
        } else {
            response.put("status", "OK");
            response.put("message", "No student in this generation");
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }


//    @GetMapping("/getAllStudentsByGeneration/advance/{generation}")
//    public ResponseEntity<?> getAllAdvanceStudentsByGeneration(@PathVariable int generation) {
//
//        GenerationDto generation1 = teacherServiceImp.getAllAdvanceStudentsByGeneration(generation);
//
//        Map<String, Object> response = new HashMap<>();
//
//        if (generation1 != null && !generation1.getStudents().isEmpty()) {
//            response.put("status", "OK");
//            response.put("message", "Get all advance students successfully!");
//            response.put("success", true);
//            response.put("payload", generation1.getStudents());
//        } else {
//            response.put("status", "OK");
//            response.put("message", "No student in this generation");
//            response.put("payload", null);
//        }
//        return ResponseEntity.ok().body(response);
//    }


    @DeleteMapping("/deleteStudentById/{id}")
    public ResponseEntity<?> deleteStudentById(@PathVariable String id) {

        Map<String, Object> response = new HashMap<>();

        if (StudentRestController.validation("ID", id)) {

            Student student = teacherServiceImp.getStudentById(id);

            if (teacherServiceImp.deleteStudentById(id) && student != null) {
                response.put("status", "OK");
                response.put("message", "Student has been deleted successfully!!");
                response.put("success", true);
                response.put("payload", student);
            } else {
                response.put("status", "OK");
                response.put("message", "Incorrect ID!");
                response.put("success", false);
                response.put("payload", null);
            }
        } else {
            response.put("status", "OK");
            response.put("message", "ID must be at least 6 characters!");
            response.put("success", false);
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }


    @PatchMapping("/updateStudentById/{id}")
    public ResponseEntity<?> updateStudentById(@PathVariable String id, @RequestBody UpdateStudentRequest student) {

        boolean check = false;

        Map<String, Object> response = new HashMap<>();

        if (StudentRestController.validation("ID", id)) {
            if (StudentRestController.validation("ID", student.getNewId())) {
                if (StudentRestController.validation("FULLNAME", student.getFullName())) {
                    if (StudentRestController.validation("EMAIL", student.getEmail())) {

                        List<String> studentIdList = studentServiceImp.getAllStudentId();

                        for (String stuId : studentIdList) {
                            if (student.getNewId().equals(stuId)) {
                                check = true;
                            }
                        }

                        if (!check) {
                            if (teacherServiceImp.updateStudentById(student, id)) {

                                Student studentInfo = teacherServiceImp.getStudentById(student.getNewId());
                                StudentResponse studentResponse = new StudentResponse();

                                studentResponse.setId(studentInfo.getId());
                                studentResponse.setFullName(studentInfo.getFullName());
                                studentResponse.setClassroom(studentInfo.getClassroom().getClassName());
                                studentResponse.setEmail(studentInfo.getEmail());
                                studentResponse.setGender(studentInfo.getGender());
                                studentResponse.setGeneration(studentInfo.getGeneration().getGeneration());
                                studentResponse.setEmail(studentInfo.getEmail());
                                studentResponse.setProfileImg(studentInfo.getProfileImg());
                                studentResponse.setCreateAt(studentInfo.getCreateAt());


                                response.put("status", "OK");
                                response.put("message", "Please let your student know that they have to login again with the new ID that you has changed!");
                                response.put("success", true);
                                response.put("payload", studentResponse);
                            } else {
                                response.put("status", "OK");
                                response.put("message", "Student's ID incorrect!");
                                response.put("success", false);
                                response.put("payload", null);
                            }
                        } else {

                            if (student.getNewId().equals(id)) {
                                if (teacherServiceImp.updateStudentById(student, id)) {

                                    Student studentInfo = teacherServiceImp.getStudentById(student.getNewId());
                                    StudentResponse studentResponse = new StudentResponse();

                                    studentResponse.setId(studentInfo.getId());
                                    studentResponse.setFullName(studentInfo.getFullName());
                                    studentResponse.setClassroom(studentInfo.getClassroom().getClassName());
                                    studentResponse.setEmail(studentInfo.getEmail());
                                    studentResponse.setGender(studentInfo.getGender());
                                    studentResponse.setGeneration(studentInfo.getGeneration().getGeneration());
                                    studentResponse.setEmail(studentInfo.getEmail());
                                    studentResponse.setProfileImg(studentInfo.getProfileImg());
                                    studentResponse.setCreateAt(studentInfo.getCreateAt());


                                    response.put("status", "OK");
                                    response.put("message", "Student has been updated successfully!");
                                    response.put("success", true);
                                    response.put("payload", studentResponse);
                                } else {
                                    response.put("status", "OK");
                                    response.put("message", "Student's ID incorrect!");
                                    response.put("success", false);
                                    response.put("payload", null);
                                }
                            } else {
                                response.put("status", "OK");
                                response.put("message", "This ID is already exist!");
                                response.put("success", false);
                                response.put("payload", null);
                            }
                        }

                    } else {
                        response.put("status", "ERROR");
                        response.put("message", "Bad email!");
                        response.put("success", false);
                        response.put("payload", null);
                    }

                } else {
                    response.put("status", "ERROR");
                    response.put("message", "Full name must be only character!");
                    response.put("success", false);
                    response.put("payload", null);
                }

            } else {
                response.put("status", "OK");
                response.put("message", "ID must be at least 6 characters!");
                response.put("success", false);
                response.put("payload", null);
            }

        } else {
            response.put("status", "OK");
            response.put("message", "ID must be at least 6 characters!");
            response.put("success", false);
            response.put("payload", null);
        }

        return ResponseEntity.ok().body(response);
    }


    @PatchMapping("/updateProfile/{username}")
    public ResponseEntity<?> updateProfile(@PathVariable String username, @RequestBody ProfileUpdateRequest profileUpdateRequest) {

        boolean check = false;

        Map<String, Object> response = new HashMap<>();

        if (StudentRestController.validation("USERNAME", username)){
            if (StudentRestController.validation("USERNAME", profileUpdateRequest.getUsername())){
                if (StudentRestController.validation("FULLNAME", profileUpdateRequest.getFullName())){
                    if (StudentRestController.validation("EMAIL", profileUpdateRequest.getEmail())){

                        List<String> usernameList = teacherServiceImp.getAllTeacherUsername();

                        for (String name : usernameList){
                            if(profileUpdateRequest.getUsername().equals(name)){
                                check = true;
                            }
                        }

                        if (!check){
                            if (teacherServiceImp.updateProfile(profileUpdateRequest, username)){

                                Teacher teacher = teacherServiceImp.loadTeacherByUsername(profileUpdateRequest.getUsername());
                                TeacherResponse teacherResponse = new TeacherResponse();

                                teacherResponse.setId(teacher.getId());
                                teacherResponse.setUsername(teacher.getUsername());
                                teacherResponse.setFullName(teacher.getFullName());
                                teacherResponse.setEmail(teacher.getEmail());
                                teacherResponse.setGender(teacher.getGender());
                                teacherResponse.setProfileImg(teacher.getProfileImg());


                                response.put("status", "OK");
                                response.put("message", "Please login again with your new username!");
                                response.put("success", true);
                                response.put("payload", teacherResponse);
                            }else {
                                response.put("status", "OK");
                                response.put("message", "Username incorrect!");
                                response.put("success", false);
                                response.put("payload", null);
                            }
                        }else {

                            if (profileUpdateRequest.getUsername().equals(username)){
                                if (teacherServiceImp.updateProfile(profileUpdateRequest, username)){

                                    Teacher teacher = teacherServiceImp.loadTeacherByUsername(username);
                                    TeacherResponse teacherResponse = new TeacherResponse();

                                    teacherResponse.setId(teacher.getId());
                                    teacherResponse.setUsername(teacher.getUsername());
                                    teacherResponse.setFullName(teacher.getFullName());
                                    teacherResponse.setEmail(teacher.getEmail());
                                    teacherResponse.setGender(teacher.getGender());
                                    teacherResponse.setProfileImg(teacher.getProfileImg());


                                    response.put("status", "OK");
                                    response.put("message", "Updated successfully!");
                                    response.put("success", true);
                                    response.put("payload", teacherResponse);
                                }else {
                                    response.put("status", "OK");
                                    response.put("message", "Username incorrect!");
                                    response.put("success", false);
                                    response.put("payload", null);
                                }
                            }else {
                                response.put("status", "OK");
                                response.put("message", "This Username is already exist!");
                                response.put("success", false);
                                response.put("payload", null);
                            }
                        }

                    }else {
                        response.put("status", "ERROR");
                        response.put("message", "Bad email!");
                        response.put("success", false);
                        response.put("payload", null);
                    }

                }else {
                    response.put("status", "ERROR");
                    response.put("message", "Full name must be only character!");
                    response.put("success", false);
                    response.put("payload", null);
                }

            }else {
                response.put("status", "ERROR");
                response.put("message", "Username must be only character and don't have space!");
                response.put("success", false);
                response.put("payload", null);
            }
        }else {
            response.put("status", "ERROR");
            response.put("message", "Username must be only character and don't have space!");
            response.put("success", false);
            response.put("payload", null);
        }


        return ResponseEntity.ok().body(response);
    }


    @DeleteMapping("/rejectStudentById/{id}")
    public ResponseEntity<?> rejectStudentById(@PathVariable String id) {

        Map<String, Object> response = new HashMap<>();

        if (StudentRestController.validation("ID", id)) {

            StudentRequest studentRequest = teacherServiceImp.getReqStuById(id);

            if (teacherServiceImp.deleteAcceptedStuById(id)) {
                response.put("status", "OK");
                response.put("message", "Reject student success!");
                response.put("success", true);
                response.put("payload", studentRequest);
            } else {
                response.put("status", "OK");
                response.put("message", "ID Incorrect!");
                response.put("payload", null);
            }
        } else {
            response.put("status", "OK");
            response.put("message", "ID must be at least 6 characters!");
            response.put("success", false);
            response.put("payload", null);
        }


        return ResponseEntity.ok().body(response);
    }


    @GetMapping("/getResultsByExamId/{examId}")
    public ResponseEntity<?> getResultsByExamId(@PathVariable String examId) {

        Map<String, Object> response = new HashMap<>();

        List<ResultResponse> resultList = examServiceImp.getSubmittedResultsByExamId(examId);

        if (!resultList.isEmpty()) {
            response.put("status", "OK");
            response.put("message", "get all results success!");
            response.put("success", true);
            response.put("payload", resultList);
        } else {
            response.put("status", "OK");
            response.put("message", "No submitted result yet!");
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }


    @PatchMapping("/correctAnswerByResultId/{id}")
    public ResponseEntity<?> correctAnswerByResultId(@PathVariable String id, float score) {

        Map<String, Object> response = new HashMap<>();

        if (examServiceImp.correctAnswerByResultId(id, score)) {
            response.put("status", "OK");
            response.put("message", "Correct result success!");
            response.put("success", true);
            response.put("payload", score);
        } else {
            response.put("status", "OK");
            response.put("message", "Correct result fail!");
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }


    @GetMapping("/viewResultById/{id}")
    public ResponseEntity<?> viewResultById(@PathVariable String id) {

        Map<String, Object> response = new HashMap<>();

        Result result = examServiceImp.viewResultById(id);

        if (result != null) {

            examServiceImp.updateResultToChecking(id);

            response.put("status", "OK");
            response.put("message", "Get results success!");
            response.put("success", true);
            response.put("payload", result);
        } else {
            response.put("status", "OK");
            response.put("message", "Get Result fail");
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }


    @GetMapping("/getCheckingResultByExamId/{id}")
    public ResponseEntity<?> getCheckingResultByExamId(@PathVariable String id) {

        Map<String, Object> response = new HashMap<>();

        List<Result> resultList = examServiceImp.CheckingResultsByExamId(id);

        if (!resultList.isEmpty()) {
            response.put("status", "OK");
            response.put("message", "Get results success!");
            response.put("success", true);
            response.put("payload", resultList);
        } else {
            response.put("status", "OK");
            response.put("message", "Get Result fail");
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }


    @GetMapping("/getAllBasicGeneration")
    public ResponseEntity<?> getAllBasicGeneration() {

        Map<String, Object> response = new HashMap<>();

        List<Generation> basicGenList = examServiceImp.getAllBasicGeneration();

        if (!basicGenList.isEmpty()) {

            response.put("status", "OK");
            response.put("message", "Get all basic generation success!");
            response.put("success", true);
            response.put("payload", basicGenList);
        } else {
            response.put("status", "OK");
            response.put("message", "No generation yet!");
            response.put("success", false);
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }


    @GetMapping("/getAllAdvanceGeneration")
    public ResponseEntity<?> getAllAdvanceGeneration() {

        Map<String, Object> response = new HashMap<>();

        List<Generation> advanceGenList = examServiceImp.getAllAdvanceGeneration();

        if (!advanceGenList.isEmpty()) {

            response.put("status", "OK");
            response.put("message", "Get all advance generation success!");
            response.put("success", true);
            response.put("payload", advanceGenList);
        } else {
            response.put("status", "OK");
            response.put("message", "No generation yet!");
            response.put("success", false);
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }


    @PatchMapping("/changePassword/{username}")
    public ResponseEntity<?> changePassword(@PathVariable String username, @RequestBody ChangePasswordRequest passwordRequest) {

        Map<String, Object> response = new HashMap<>();

        boolean isPasswordMatch;

        if (StudentRestController.validation("USERNAME", username)){
            if (StudentRestController.validation("PASSWORD", passwordRequest.getOldPassword())){
                if (StudentRestController.validation("PASSWORD", passwordRequest.getNewPassword())){

                    if (passwordRequest.getNewPassword().equals(passwordRequest.getOldPassword())){
                        response.put("status", "OK");
                        response.put("message", "New password and current password is the same!");
                        response.put("success", false);
                        response.put("payload", passwordRequest);
                    }else {

                        String oldPassword = teacherServiceImp.getOldPassword(username);

                        if (oldPassword == null){
                            response.put("status", "OK");
                            response.put("message", "Incorrect username!");
                            response.put("success", false);
                            response.put("payload", null);
                        }else {

                            isPasswordMatch = passwordEncoder.matches(passwordRequest.getOldPassword(), oldPassword);

                            if (isPasswordMatch){

                                if (teacherServiceImp.changePassword(username, passwordEncoder.encode(passwordRequest.getNewPassword()))){

                                    response.put("status", "OK");
                                    response.put("message", "Password changed successfully!");
                                    response.put("success", true);
                                    response.put("payload", passwordRequest.getNewPassword());

                                }else {

                                    response.put("status", "OK");
                                    response.put("message", "Change password fail!");
                                    response.put("success", false);
                                    response.put("payload", null);
                                }

                            }else {
                                response.put("status", "OK");
                                response.put("message", "Incorrect current password!");
                                response.put("success", false);
                                response.put("payload", null);
                            }
                        }
                    }
                }else {
                    response.put("status", "ERROR");
                    response.put("message", "New password must be at least 6 character!");
                    response.put("success", false);
                    response.put("payload", null);
                }
            } else {
                response.put("status", "ERROR");
                response.put("message", "Current password must be at least 6 character!");
                response.put("success", false);
                response.put("payload", null);
            }
        } else {
            response.put("status", "ERROR");
            response.put("message", "Username must be only character and don't have space!");
            response.put("success", false);
            response.put("payload", null);
        }

        return ResponseEntity.ok().body(response);
    }


    @GetMapping("/getProfile")
    public ResponseEntity<?> getProfile() {

        Map<String, Object> response = new HashMap<>();

        Teacher teacher = teacherServiceImp.loadTeacherByUsername(getCurrentUser());

        if (teacher != null) {

            TeacherResponse teacherResponse = new TeacherResponse();

            teacherResponse.setId(teacher.getId());
            teacherResponse.setUsername(teacher.getUsername());
            teacherResponse.setFullName(teacher.getFullName());
            teacherResponse.setEmail(teacher.getEmail());
            teacherResponse.setGender(teacher.getGender());
            teacherResponse.setProfileImg(teacher.getProfileImg());

            response.put("status", "OK");
            response.put("message", "Get profile success!");
            response.put("success", true);
            response.put("payload", teacherResponse);
        } else {
            response.put("status", "OK");
            response.put("message", "Get profile fail");
            response.put("success", false);
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }

    @GetMapping("/getAllStudentsInCurrentGeneration")
    public ResponseEntity<?> getAllStudentsInCurrentGeneration() {

        Map<String, Object> response = new HashMap<>();
        int currentGenId = teacherServiceImp.getCurrentGenId();
        List<StudentResponse> studentList = teacherServiceImp.getAllStudentsInCurrentGen(currentGenId);

        if (!studentList.isEmpty()) {
            response.put("status", "OK");
            response.put("message", "Get all students success!");
            response.put("success", true);
            response.put("payload", studentList);
        } else {
            response.put("status", "OK");
            response.put("message", "No student in this class!");
            response.put("success", false);
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }



    @GetMapping("/getAllGeneration")
    public ResponseEntity<?> getAllGeneration() {

        Map<String, Object> response = new HashMap<>();
        List<Generation> generationsList = teacherServiceImp.getAllGenerations();

        if (!generationsList.isEmpty()) {
            response.put("status", "OK");
            response.put("message", "Get all generations successfully!");
            response.put("success", true);
            response.put("payload", generationsList);
        } else {
            response.put("status", "OK");
            response.put("message", "Get all generations !");
            response.put("success", false);
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }

}