package com.kshrd.hrdexam.controller.teacher;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kshrd.hrdexam.controller.student.StudentRestController;
import com.kshrd.hrdexam.model.*;
import com.kshrd.hrdexam.payload.dto.teacherDto.*;
import com.kshrd.hrdexam.payload.request.teacherRequest.*;
import com.kshrd.hrdexam.service.studentService.StudentServiceImp;
import com.kshrd.hrdexam.service.teacherService.ExamServiceImp;
import com.kshrd.hrdexam.service.teacherService.TeacherServiceImp;
import org.modelmapper.internal.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/exam")
public class ExamRestController {

    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
    Date date = new Date();

    @Autowired
    private ExamServiceImp examServiceImp;

    @Autowired
    private TeacherServiceImp teacherServiceImp;

    @Autowired
    private StudentServiceImp studentServiceImp;

    @Autowired
    private ObjectMapper objectMapper;

    @PostMapping("/create")
    public ResponseEntity<?> createExam(@RequestBody ExamRequest examRequest){

        Map<String, Object> response = new HashMap<>();


        if (StudentRestController.validation("NOT_EMPTY", examRequest.getExamName())) {
            if (StudentRestController.validation("NOT_EMPTY", examRequest.getExamSubject())) {
                if (StudentRestController.validation("NOT_EMPTY", examRequest.getExamDate())) {

                    try{

                        Teacher teacher = teacherServiceImp.loadTeacherByUsername(TeacherRestController.getCurrentUser());
                        Generation generation = teacherServiceImp.getGenerationNameById(teacherServiceImp.getCurrentGenId());

                        Exam exam = new Exam();

                        exam.setId(RandomString.make(20));
                        exam.setExamSubject(examRequest.getExamSubject());
                        exam.setExamDate(examRequest.getExamDate());
                        exam.setExamName(examRequest.getExamName());
                        exam.setTotalScore(examRequest.getTotalScore());
                        exam.setTeacher(teacher);
                        exam.setDuration(examRequest.getDuration());
                        exam.setGeneration(generation);
                        exam.setStatus(0);
                        exam.setAssignType(0);

                        ExamResponse examResponse = new ExamResponse();

                        examResponse.setId(exam.getId());
                        examResponse.setExamSubject(examRequest.getExamSubject());
                        examResponse.setExamDate(examRequest.getExamDate());
                        examResponse.setExamName(examRequest.getExamName());
                        examResponse.setTotalScore(examRequest.getTotalScore());
                        examResponse.setTeacher(teacher.getFullName());
                        examResponse.setDuration(examRequest.getDuration());
                        examResponse.setGeneration(generation.getGeneration());
                        examResponse.setStatus(0);
                        examResponse.setAssignType(0);

                        if (examServiceImp.createExam(exam)) {
                            response.put("status", "OK");
                            response.put("message", "Exam has created successfully!");
                            response.put("success", true);
                            response.put("payload", examResponse);
                        } else {
                            response.put("status", "OK");
                            response.put("message", "Fail to create exam!");
                            response.put("success", false);
                            response.put("payload", null);
                        }
                    }catch (Exception e){
                        response.put("status", "OK");
                        response.put("message", "Duration and score must be only number!");
                        response.put("success", false);
                        response.put("payload", null);
                    }

                }else {
                    response.put("status", "OK");
                    response.put("message", "Exam date can not be empty!");
                    response.put("success", false);
                    response.put("payload", null);
                }
            }else {
                response.put("status", "OK");
                response.put("message", "Exam subject can not be empty!");
                response.put("success", false);
                response.put("payload", null);
            }
        }else {
            response.put("status", "OK");
            response.put("message", "Exam title can not be empty!");
            response.put("success", false);
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }




    @GetMapping("/getAllUnassignedExam")
    public ResponseEntity<?> getAllUnassignedExam(){

        Map<String, Object> response = new HashMap<>();
        List<ExamResponse> examResponseList = new ArrayList<>();

        List<Exam> examList = examServiceImp.getAllUnassignedExam();

        if (!examList.isEmpty()){

            for (Exam exam : examList){
                examResponseList.add(new ExamResponse(
                        exam.getId(),
                        exam.getExamSubject(),
                        exam.getExamDate(),
                        exam.getExamName(),
                        exam.getTotalScore(),
                        exam.getTeacher().getFullName(),
                        exam.getDuration(),
                        exam.getGeneration().getGeneration(),
                        exam.getStatus(),
                        exam.getAssignType()
                ));
            }

            response.put("status", "OK");
            response.put("message", "Get unassigned exam successfully!");
            response.put("success", true);
            response.put("payload", examResponseList);
        }else {
            response.put("status", "OK");
            response.put("message", "No exam");
            response.put("success", false);
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }


    @GetMapping("/getAllAssignedExamByGen/basic/{generation}")
    public ResponseEntity<?> getAllBasicAssignedExamByGen(@PathVariable int generation){

        Map<String, Object> response = new HashMap<>();
        List<ExamResponse> examResponseList = new ArrayList<>();

        ExamGenerationDto exam = examServiceImp.getAllAssignedExamByGen(generation);

        if (exam != null && !exam.getExamList().isEmpty()){

            for (Exam examInfo : exam.getExamList()){
                examResponseList.add(new ExamResponse(
                        examInfo.getId(),
                        examInfo.getExamSubject(),
                        examInfo.getExamDate(),
                        examInfo.getExamName(),
                        examInfo.getTotalScore(),
                        examInfo.getTeacher().getFullName(),
                        examInfo.getDuration(),
                        examInfo.getGeneration().getGeneration(),
                        examInfo.getStatus(),
                        examInfo.getAssignType()
                ));
            }


            response.put("status", "OK");
            response.put("message", "Get all basic exam successfully!");
            response.put("success", true);
            response.put("payload", examResponseList);
        }else {
            response.put("status", "OK");
            response.put("message", "No exam in this generation!");
            response.put("success", false);
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }


    @GetMapping("/getAllAssignedExamByGen/advance/{generation}")
    public ResponseEntity<?> getAllAdvanceAssignedExamByGen(@PathVariable int generation){

        Map<String, Object> response = new HashMap<>();
        List<ExamResponse> examResponseList = new ArrayList<>();

        ExamGenerationDto examAdvance = examServiceImp.getAllAdvanceAssignedExamByGen(generation);

        if (examAdvance != null && !examAdvance.getExamList().isEmpty()){

            for (Exam examInfo : examAdvance.getExamList()){
                examResponseList.add(new ExamResponse(
                        examInfo.getId(),
                        examInfo.getExamSubject(),
                        examInfo.getExamDate(),
                        examInfo.getExamName(),
                        examInfo.getTotalScore(),
                        examInfo.getTeacher().getFullName(),
                        examInfo.getDuration(),
                        examInfo.getGeneration().getGeneration(),
                        examInfo.getStatus(),
                        examInfo.getAssignType()
                ));
            }


            response.put("status", "OK");
            response.put("message", "Get all advance exam successfully!");
            response.put("success", true);
            response.put("payload", examResponseList);
        }else {
            response.put("status", "OK");
            response.put("message", "No exam in this generation!");
            response.put("success", false);
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }




    @PostMapping("/create/section")
    public ResponseEntity<?> createSection(@RequestBody SectionRequest sectionRequest){

        boolean checkSectionOder = false;

        Map<String, Object> response = new HashMap<>();

        if (StudentRestController.validation("NOT_EMPTY", sectionRequest.getHeading())){
            if (StudentRestController.validation("NOT_EMPTY", sectionRequest.getExamId())){

                List<Integer> sectionOrderList = examServiceImp.getSectionOrderByExamId(sectionRequest.getExamId());

                if (!sectionOrderList.isEmpty()){
                    for (Integer order : sectionOrderList){
                        if (order == sectionRequest.getSection()){
                            checkSectionOder = true;
                        }
                    }
                }

                    if (checkSectionOder){

                        response.put("status", "OK");
                        response.put("message", "Duplicate section order!");
                        response.put("success", false);
                        response.put("payload", null);

                    }else {
                        Section section = new Section();
                        section.setSection(sectionRequest.getSection());
                        section.setId(RandomString.make(20));
                        section.setHeading(sectionRequest.getHeading());
                        section.setExamId(sectionRequest.getExamId());

                        if (examServiceImp.createSection(section)){
                            response.put("status", "OK");
                            response.put("message", "Section created successfully!");
                            response.put("success", true);
                            response.put("payload", section);
                        }else {
                            response.put("status", "OK");
                            response.put("message", "Section order must be only number!");
                            response.put("success", false);
                            response.put("payload", null);
                        }
                    }



            }else {
                response.put("status", "OK");
                response.put("message", "Exam ID can not be empty!");
                response.put("success", false);
                response.put("payload", null);
            }
        }else {
            response.put("status", "OK");
            response.put("message", "Section heading can not be empty!");
            response.put("success", false);
            response.put("payload", null);
        }


        return ResponseEntity.ok().body(response);
    }



    @GetMapping("/getSectionByExamId/{id}")
    public ResponseEntity<?> getSectionByExamId(@PathVariable String id){

        Map<String, Object> response = new HashMap<>();

        List<Section> sectionList = examServiceImp.getSectionByExamId(id);

        if (!sectionList.isEmpty()){

            Collections.sort(sectionList);

            response.put("status", "OK");
            response.put("message", "Get section successfully!");
            response.put("success", true);
            response.put("payload", sectionList);
        }else {
            response.put("status", "OK");
            response.put("message", "Get section fail!");
            response.put("success", false);
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }



    @PutMapping("/updateExamById/{id}")
    public ResponseEntity<?> updateExamById(@PathVariable String id, @RequestBody ExamRequest examRequest){

        Map<String, Object> response = new HashMap<>();

        if (StudentRestController.validation("NOT_EMPTY", examRequest.getExamName())) {
            if (StudentRestController.validation("NOT_EMPTY", examRequest.getExamSubject())) {
                if (StudentRestController.validation("NOT_EMPTY", examRequest.getExamDate())) {

                    if (examServiceImp.updateExam(id, examRequest)){
                        response.put("status", "OK");
                        response.put("message", "Exam update successfully!");
                        response.put("success", true);
                        response.put("payload", examRequest);
                    }else {
                        response.put("status", "OK");
                        response.put("message", "Incorrect exam ID");
                        response.put("success", false);
                        response.put("payload", null);
                    }

                }else {
                    response.put("status", "OK");
                    response.put("message", "Exam date can not be empty!");
                    response.put("success", false);
                    response.put("payload", null);
                }
            }else {
                response.put("status", "OK");
                response.put("message", "Exam subject can not be empty!");
                response.put("success", false);
                response.put("payload", null);
            }
        }else {
            response.put("status", "OK");
            response.put("message", "Exam title can not be empty!");
            response.put("success", false);
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }





    @PutMapping("/updateSections")
    public ResponseEntity<?> updateSection(@RequestBody List<SectionResponse> sectionResponseList){

        Map<String, Object> response = new HashMap<>();
        List<Section> sectionList = new ArrayList<>();
        List<QuestionRequest> questionList = new ArrayList<>();

        if (!sectionResponseList.isEmpty()){

            for (SectionResponse section : sectionResponseList){
                sectionList.add(new Section(section.getId(), section.getHeading(), section.getExamId(), section.getSection()));
                for (QuestionResponse question : section.getQuestionResponses()) {
                    questionList.add(new QuestionRequest(
                            question.getId(),
                            question.getSectionId(),
                            question.getQuestionTypeId(),
                            question.getQuestionContent(),
                            question.getImage(),
                            question.getScore()
                    ));
                }
            }


            for (Section section : sectionList){
                examServiceImp.updateSection(section.getId(), section);
            }


            for (QuestionRequest question : questionList){
                examServiceImp.updateQuestion(question.getId(), question);
            }

            response.put("status", "OK");
            response.put("message", "Section updated successfully!");
            response.put("success", true);
            response.put("payload", sectionList);
        }else {
            response.put("status", "OK");
            response.put("message", "Fail to update section!");
            response.put("success", false);
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }



    @DeleteMapping("/deleteExamById/{id}")
    public ResponseEntity<?> deleteExamById(@PathVariable String id){

        Map<String, Object> response = new HashMap<>();

        Exam exam = examServiceImp.getExamById(id);
        int status = studentServiceImp.getExamStatusById(id);

        if (status == 1){
            response.put("status", "OK");
            response.put("message", "You can't delete active exam!");
            response.put("success", false);
            response.put("payload", null);
        }else {
            if (examServiceImp.deleteExamById(id) && exam != null){
                response.put("status", "OK");
                response.put("message", "Exam deleted successfully!");
                response.put("success", true);
                response.put("payload", exam);
            }else {
                response.put("status", "OK");
                response.put("message", "Incorrect exam ID!");
                response.put("success", false);
                response.put("payload", null);
            }
        }
        return ResponseEntity.ok().body(response);
    }





    @DeleteMapping("/deleteSectionById/{id}")
    public ResponseEntity<?> deleteSectionById(@PathVariable String id){

        Map<String, Object> response = new HashMap<>();

        Section section = examServiceImp.getSectionById(id);

        if (examServiceImp.deleteSectionById(id) && section != null){
            response.put("status", "OK");
            response.put("message", "Section deleted successfully!");
            response.put("success", true);
            response.put("payload", section);
        }else {
            response.put("status", "OK");
            response.put("message", "Fail to delete section!");
            response.put("success", false);
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }




    @PostMapping("/create/question")
    public ResponseEntity<?> createQuestion(@RequestBody QuestionRequest questionRequest){

        Map<String, Object> response = new HashMap<>();

        if (StudentRestController.validation("NOT_EMPTY", questionRequest.getSectionId())) {
            if (StudentRestController.validation("NOT_EMPTY", questionRequest.getQuestionTypeId())) {
                if (StudentRestController.validation("NOT_EMPTY", questionRequest.getQuestionContent())) {

                    Question question = new Question();
                    question.setId(RandomString.make(20));
                    question.setSectionId(questionRequest.getSectionId());
                    question.setQuestionTypeId(questionRequest.getQuestionTypeId());
                    question.setQuestionContent(questionRequest.getQuestionContent());
                    question.setImage(questionRequest.getImage());
                    question.setScore(questionRequest.getScore());

                    if (examServiceImp.createQuestion(question)){
                        response.put("status", "OK");
                        response.put("message", "Question has created successfully!");
                        response.put("success", true);
                        response.put("payload", question);
                    }else {
                        response.put("status", "OK");
                        response.put("message", "Fail to create question!");
                        response.put("success", false);
                        response.put("payload", null);
                    }

                }else {
                    response.put("status", "OK");
                    response.put("message", "Question content can not be empty!");
                    response.put("success", false);
                    response.put("payload", null);
                }
            }else {
                response.put("status", "OK");
                response.put("message", "Question type ID can not be empty!");
                response.put("success", false);
                response.put("payload", null);
            }
        }else {
            response.put("status", "OK");
            response.put("message", "Section ID can not be empty!");
            response.put("success", false);
            response.put("payload", null);
        }

        return ResponseEntity.ok().body(response);
    }




    @GetMapping("/getQuestionBySectionId/{id}")
    public ResponseEntity<?> getQuestionBySectionId(@PathVariable String id){

        Map<String, Object> response = new HashMap<>();

        List<Question> questionList = examServiceImp.getQuestionBySectionId(id);

        if (!questionList.isEmpty()){
            response.put("status", "OK");
            response.put("message", "Get questions successfully!");
            response.put("success", true);
            response.put("payload", questionList);
        }else {
            response.put("status", "OK");
            response.put("message", "Incorrect section ID!");
            response.put("success", false);
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }



    @DeleteMapping("/deleteQuestionById/{id}")
    public ResponseEntity<?> deleteQuestionById(@PathVariable String id){

        Map<String, Object> response = new HashMap<>();

        Question question = examServiceImp.getQuestionById(id);

        if (question != null && examServiceImp.deleteQuestionById(id)){
            response.put("status", "OK");
            response.put("message", "Question has been deleted successfully!");
            response.put("success", true);
            response.put("payload", question);
        }else {
            response.put("status", "OK");
            response.put("message", "Incorrect ID!");
            response.put("success", false);
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }


    @PutMapping("/updateQuestions")
    public ResponseEntity<?> updateQuestions(@RequestBody List<QuestionResponse> questionResponses){

        Map<String, Object> response = new HashMap<>();

        if (!questionResponses.isEmpty()){
            response.put("status", "OK");
            response.put("message", "Question has been updated successfully!");
            response.put("success", true);
            response.put("payload", questionResponses);
        }else {
            response.put("status", "OK");
            response.put("message", "You haven't update any question yet!");
            response.put("success", false);
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }




    @PostMapping("/create/answer")
    public ResponseEntity<?> createAnswer(@RequestBody AnswerRequest answerRequest){

        Map<String, Object> response = new HashMap<>();
        Answer answer = new Answer();
        answer.setQuestionId(answerRequest.getQuestionId());
        answer.setAnswer(answerRequest.getAnswer());

        if (examServiceImp.createAnswer(answer)){
            try {
                String jsonAnswer = objectMapper.writeValueAsString(answer.getAnswer());
                answer.setAnswer(jsonAnswer);

                response.put("status", "OK");
                response.put("message", "Answer has been created successfully!");
                response.put("success", true);
                response.put("payload", answer);

            }catch (JsonProcessingException e){
                e.printStackTrace();
            }


        }else {
            response.put("status", "OK");
            response.put("message", "Fail to create answer!");
            response.put("success", false);
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }



    @PostMapping("/assign/assignAllStudents")
    public ResponseEntity<?> assignAllStudents(@RequestBody AssignAllRequest assignAllRequest){

        boolean isAssign = true;

        Map<String, Object> response = new HashMap<>();

        if (StudentRestController.validation("NOT_EMPTY", assignAllRequest.getExamId())){

            if (checkExamBeforeAssign()){

                GenerationDto generationDto = examServiceImp.getAllStudentsByGeneration();

                   if (generationDto != null) {

                    for (Student student : generationDto.getStudents()) {
                        examServiceImp.assignAllStudents(assignAllRequest.getExamId(), student.getId());
                    }

                    examServiceImp.activateExam(assignAllRequest.getExamId());

                    response.put("status", "OK");
                    response.put("message", "Assign all students successfully!");
                    response.put("success", true);
                    response.put("payload", generationDto.getStudents());


                } else {
                    response.put("status", "OK");
                    response.put("message", "Assign all students fail");
                    response.put("success", false);
                    response.put("payload", null);
                }

            }else {
                response.put("status", "OK");
                response.put("message", "Sorry, You can't assign an exam during another exam is active!");
                response.put("success", false);
                response.put("payload", null);
            }

//            List<AssignAll> assignAllList = examServiceImp.getIdAssignedStudent();
//            List<AssignClass> getAssignedClassList = examServiceImp.getAssignedClass();
//
//            if (assignAllList.isEmpty() && getAssignedClassList.isEmpty()) {
//                GenerationDto generationDto = examServiceImp.getAllStudentsByGeneration();
//
//                if (generationDto != null) {
//
//                    for (Student student : generationDto.getStudents()) {
//                        examServiceImp.assignAllStudents(assignAllRequest.getExamId(), student.getId());
//                    }
//
////                if (isAssign){
//                    examServiceImp.activateExam(assignAllRequest.getExamId());
//
//                    response.put("status", "OK");
//                    response.put("message", "Assign all students success!");
//                    response.put("success", true);
//                    response.put("payload", generationDto.getStudents());
//
////                }else {
////
////                    response.put("status", "ERROR");
////                    response.put("message", "Incorrect exam ID!   ");
////                    response.put("success", false);
////                    response.put("payload", null);
////                }
//
//
//                } else {
//                    response.put("status", "OK");
//                    response.put("message", "Assign all students fail");
//                    response.put("success", false);
//                    response.put("payload", null);
//                }
//            }else {
//                response.put("status", "OK");
//                response.put("message", "Some students has assigned by other teacher!");
//                response.put("success", false);
//                response.put("payload", null);
//            }
        }else {
            response.put("status", "ERROR");
            response.put("message", "Exam ID can not be empty!");
            response.put("success", false);
            response.put("payload", null);
        }


        return ResponseEntity.ok().body(response);
    }




    @PostMapping("/assign/specificStudent")
    public ResponseEntity<?> assignSpecificStudent(@RequestBody AssignSpecificStudentRequest studentRequest){

        List<String> assignedStu = new ArrayList<>();

        Map<String, Object> response = new HashMap<>();


        if (checkExamBeforeAssign()){

            if (!studentRequest.getStudentIdList().isEmpty()){
                for (String id : studentRequest.getStudentIdList()){
                    examServiceImp.assignAllStudents(studentRequest.getExamId(), id);
                }

                examServiceImp.activateExam(studentRequest.getExamId());

                response.put("status", "OK");
                response.put("message", "Assign specific students successfully!");
                response.put("success", true);
                response.put("payload", studentRequest.getStudentIdList());

            }else {
                response.put("status", "OK");
                response.put("message", "You haven't choose any student yet!");
                response.put("success", false);
                response.put("payload", null);
            }


        }else {
            response.put("status", "OK");
            response.put("message", "Sorry, You can't assign an exam during another exam is active!");
            response.put("success", false);
            response.put("payload", null);
        }

//        List<AssignAll> assignAllList = examServiceImp.getIdAssignedStudent();
//        List<AssignClass> getAssignedClassList = examServiceImp.getAssignedClass();
//
//        if (!getAssignedClassList.isEmpty()){
//            for (int i=0; i<getAssignedClassList.size(); i++){
//                for (String id : studentId){
//                    int classId = examServiceImp.getClassIdByStuId(id);
//                    if (getAssignedClassList.get(i).getClassId() == classId){
//                        assignedStu.add(id);
//                    }
//                }
//            }
//        }
//
//
//        if (!assignAllList.isEmpty()){
//            for (int i=0; i<assignAllList.size(); i++){
//                for (String stuId : studentId){
//                    if (assignAllList.get(i).getStudentId().equals(stuId)){
//                        assignedStu.add(stuId);
//                    }
//                }
//            }
//        }
//
//
//        if (assignedStu.isEmpty()){
//
//            if (studentId != null){
//                for (String id : studentId){
//                    examServiceImp.assignAllStudents(examId, id);
//                }
//            }
//
//            examServiceImp.activateExam(examId);
//
//            response.put("status", "OK");
//            response.put("message", "Assign success!");
//            response.put("success", true);
//            response.put("payload", studentId);
//        }else {
//            response.put("status", "OK");
//            response.put("message", "These students has assigned!");
//            response.put("success", false);
//            response.put("payload", assignedStu);
//        }

        return ResponseEntity.ok().body(response);
    }



    @PostMapping("/assign/specificClass")
    public ResponseEntity<?> assignSpecificClass(@RequestBody AssignClassRequest classRequest){

        Map<String, Object> response = new HashMap<>();

        boolean success = false;

        if (checkExamBeforeAssign()){

            if (classRequest.getClassId().isEmpty()){
                response.put("status", "OK");
                response.put("message", "You haven't choose any class or student yet!");
                response.put("success", false);
                response.put("payload", null);
            }else {

            if (classRequest.getClassId() != null){
                for (Integer classId : classRequest.getClassId()){
                    examServiceImp.assignToClass(classRequest.getExamId(), classId);
                }

                success = true;
            }

            if ( classRequest.getStudentId() != null){
                for (String stuId :  classRequest.getStudentId()){
                    examServiceImp.assignAllStudents(classRequest.getExamId(), stuId);
                }

                success = true;
            }

            if (success){

                examServiceImp.activateExam(classRequest.getExamId());

                response.put("status", "OK");
                response.put("message", "Assign specific class successfully!");
                response.put("success", true);
                response.put("payload", classRequest);

            }else {
                response.put("status", "OK");
                response.put("message", "You haven't choose any class or student yet!");
                response.put("success", false);
                response.put("payload", null);
            }
        }


        }else {
            response.put("status", "OK");
            response.put("message", "Sorry, You can't assign an exam during another exam is active!");
            response.put("success", false);
            response.put("payload", null);
        }

//        List<Integer> assignedClass = new ArrayList<>();
//        List<String> assignedStu = new ArrayList<>();
//
//        List<AssignAll> assignAllList = examServiceImp.getIdAssignedStudent();
//        List<AssignClass> getAssignedClassList = examServiceImp.getAssignedClass();
//
//        if (!getAssignedClassList.isEmpty()){
//            for (int i=0; i<getAssignedClassList.size(); i++){
//                for (Integer classId : classRequest.getClassId()){
//                    if (getAssignedClassList.get(i).getClassId() == classId){
//                        assignedClass.add(classId);
//                    }
//                }
//            }
//        }else {
//            response.put("status", "OK");
//            response.put("message", "Assign fail!");
//            response.put("success", false);
//            response.put("payload", null);
//        }
//
//        if (!assignAllList.isEmpty()){
//            for (int i=0; i<assignAllList.size(); i++){
//                if(classRequest.getStudentId() != null){
//                    for (String stuId : classRequest.getStudentId()){
//                        if (assignAllList.get(i).getStudentId().equals(stuId)){
//                            assignedStu.add(stuId);
//                        }
//                    }
//                }
//            }
//
//        }else {
//            response.put("status", "OK");
//            response.put("message", "Assign fail!");
//            response.put("success", false);
//            response.put("payload", null);
//        }
//
//        if(classRequest.getClassId() != null){
//            for (int i=0; i<classRequest.getClassId().size(); i++){
//                for (String stuId : classRequest.getStudentId()){
//                    int classId = examServiceImp.getClassIdByStuId(stuId);
//                    if (classId == classRequest.getClassId().get(i)){
//                        assignedStu.add(stuId);
//                    }
//                }
//            }
//        }
//
//
//        if(classRequest.getClassId() != null){
//            for (int i=0; i<classRequest.getClassId().size(); i++){
//                for (AssignAll stu : assignAllList){
//                    int classId = examServiceImp.getClassIdByStuId(stu.getStudentId());
//                    if (classId == classRequest.getClassId().get(i)){
//                        assignedClass.add(classId);
//                    }
//                }
//            }
//        }
//
//
//        if (assignedStu.isEmpty() && assignedClass.isEmpty()){
//
//            if ( classRequest.getStudentId() != null){
//                for (String stuId :  classRequest.getStudentId()){
//                    examServiceImp.assignAllStudents(examId, stuId);
//                }
//            }
//
//            if (classRequest.getClassId() != null){
//                for (Integer classId : classRequest.getClassId()){
//                    examServiceImp.assignToClass(examId, classId);
//                }
//            }
//
//            examServiceImp.activateExam(examId);
//
//            response.put("status", "OK");
//            response.put("message", "Assign success!");
//            response.put("success", true);
//            response.put("payload", classRequest.getClassId());
//        }else if (!assignedStu.isEmpty()){
//            response.put("status", "OK");
//            response.put("message", "These student has assigned!");
//            response.put("success", false);
//            response.put("payload", assignedStu);
//        }else {
//            response.put("status", "OK");
//            response.put("message", "These class has assigned!");
//            response.put("success", false);
//            response.put("payload", assignedClass);
//        }
        return ResponseEntity.ok().body(response);
    }



    @PutMapping("/deactivateExam")
    public ResponseEntity<?> deactivateExam(@RequestBody DeactivateExamRequest deactivateExamRequest){
        System.out.println("ID "+deactivateExamRequest.getExamId());
        Map<String, Object> response = new HashMap<>();

        if (examServiceImp.deactivateExam(deactivateExamRequest.getExamId())){
            response.put("status", "OK");
            response.put("message", "Deactivate exam success!");
            response.put("success", true);
            response.put("payload", deactivateExamRequest.getExamId());
        }else {
            response.put("status", "OK");
            response.put("message", "Incorrect exam ID!");
            response.put("success", false);
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }




    @GetMapping("/getExamById/{id}")
    public ResponseEntity<?> getExamById(@PathVariable String id){

        Map<String, Object> response = new HashMap<>();

        ExamPaper exam = examServiceImp.getExamPaperById(id);

        if (exam != null){
            response.put("status", "OK");
            response.put("message", "Get exam success!");
            response.put("success", true);
            response.put("payload", exam);
        }else {
            response.put("status", "OK");
            response.put("message", "No exam");
            response.put("success", false);
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }


    @GetMapping("/getAllAssignTypes")
    public ResponseEntity<?> getAllAssignTypes(){

        Map<String, Object> response = new HashMap<>();

        List<AssignType> assignTypeList = examServiceImp.getAllAssignTypes();

        if (!assignTypeList.isEmpty()){
            response.put("status", "OK");
            response.put("message", "Get all assign types successfully!");
            response.put("success", true);
            response.put("payload", assignTypeList);
        }else {
            response.put("status", "OK");
            response.put("message", "Get all assign types fail!");
            response.put("success", false);
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }



    @PutMapping("/updateAnswer")
    public ResponseEntity<?> updateAnswer(@RequestBody UpdateAnswerRequest answer){

        Map<String, Object> response = new HashMap<>();

        if (examServiceImp.updateAnswer(answer)){

            try {

                String jsonAnswer = objectMapper.writeValueAsString(answer.getAnswer());
                answer.setAnswer(jsonAnswer);

                response.put("status", "OK");
                response.put("message", "Answer has been updated successfully!");
                response.put("success", true);
                response.put("payload", answer);

            }catch (JsonProcessingException e){
                e.printStackTrace();
            }

        }else {
            response.put("status", "OK");
            response.put("message", "Fail to update answer!");
            response.put("success", false);
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }


    @PutMapping("/updateQuestionTypeByQuestionId/{id}")
    public ResponseEntity<?> updateQuestionTypeByQuestionId(@PathVariable String id,@RequestBody String questionTypeId){

        Map<String, Object> response = new HashMap<>();

        if (examServiceImp.updateQuestionTypeByQuestionId(id, questionTypeId)){
            response.put("status", "OK");
            response.put("message", "Question type ID has been updated successfully!");
            response.put("success", true);
            response.put("payload", true);
        }else {
            response.put("status", "OK");
            response.put("message", "Question type ID update fail!");
            response.put("success", false);
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }


    @DeleteMapping("/deleteAnswerByQuestionId/{id}")
    public ResponseEntity<?> deleteAnswerByQuestionId(@PathVariable String id){

        Map<String, Object> response = new HashMap<>();

        if (examServiceImp.deleteAnswerByQuestionId(id)){
            response.put("status", "OK");
            response.put("message", "Answer has been deleted successfully!");
            response.put("success", true);
            response.put("payload", id);
        }else {
            response.put("status", "OK");
            response.put("message", "Answer delete fail!");
            response.put("success", false);
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }

    public boolean checkExamBeforeAssign(){

        Exam exam = examServiceImp.getExam();

        if (exam == null){
            return true;
        }else {
            return false;
        }

    }


    @PutMapping("/updateAssignTypeByExamId/{examId}")
    public ResponseEntity<?> updateAssignTypeByExamId(@PathVariable String examId, @RequestBody UpdateAssignTypeRequest updateAssignTypeRequest){

        Map<String, Object> response = new HashMap<>();

        if (examServiceImp.updateExamAssignTypeId(examId, updateAssignTypeRequest.getNewType())){
            response.put("status", "OK");
            response.put("message", "Assign type has been updated successfully!");
            response.put("success", true);
            response.put("payload", updateAssignTypeRequest.getNewType());
        }else {
            response.put("status", "OK");
            response.put("message", "Fail to update assign type!");
            response.put("success", false);
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }


    @GetMapping("/getAllAssignedExam")
    public ResponseEntity<?> getAllAssignedExam(){

        Map<String, Object> response = new HashMap<>();
        List<ExamResponse> examResponseList = new ArrayList<>();

        List<Exam> examList = examServiceImp.getAllAssignedExam();

        if (!examList.isEmpty()){

            for (Exam exam : examList){
                examResponseList.add(new ExamResponse(
                        exam.getId(),
                        exam.getExamSubject(),
                        exam.getExamDate(),
                        exam.getExamName(),
                        exam.getTotalScore(),
                        exam.getTeacher().getFullName(),
                        exam.getDuration(),
                        exam.getGeneration().getGeneration(),
                        exam.getStatus(),
                        exam.getAssignType()
                ));
            }

            response.put("status", "OK");
            response.put("message", "Get all assigned exam successfully!");
            response.put("success", true);
            response.put("payload", examResponseList);
        }else {
            response.put("status", "OK");
            response.put("message", "No exam");
            response.put("success", false);
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }


    @PutMapping("/saveResult")
    public ResponseEntity<?> saveResult(@RequestBody SaveResultRequest resultRequest){

        Map<String, Object> response = new HashMap<>();

        if (examServiceImp.saveResult(resultRequest)){
            response.put("status", "OK");
            response.put("message", "Result has been saved successfully!");
            response.put("success", true);
            response.put("payload", resultRequest);
        }else {
            response.put("status", "OK");
            response.put("message", "Fail to saved result!");
            response.put("success", false);
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }


    @PutMapping("/turnInResultByExamId")
    public ResponseEntity<?> turnInResultByExamId(@RequestBody TurnInRequestion turnInRequestion){

        Map<String, Object> response = new HashMap<>();

        if (examServiceImp.turnInResult(turnInRequestion.getExamId())){
            response.put("status", "OK");
            response.put("message", "Result has been turned in successfully!");
            response.put("success", true);
            response.put("payload", true);
        }else {
            response.put("status", "OK");
            response.put("message", "Fail to turn in result!");
            response.put("success", false);
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }

}
