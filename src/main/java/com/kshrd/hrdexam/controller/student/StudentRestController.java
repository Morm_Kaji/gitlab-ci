package com.kshrd.hrdexam.controller.student;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kshrd.hrdexam.controller.teacher.TeacherRestController;
import com.kshrd.hrdexam.model.*;
import com.kshrd.hrdexam.payload.dto.studentDto.ResultDto;
import com.kshrd.hrdexam.payload.dto.studentDto.StudentLoginDetail;
import com.kshrd.hrdexam.payload.dto.studentDto.StudentResponse;
import com.kshrd.hrdexam.payload.dto.studentDto.StudentUpdateRequest;
import com.kshrd.hrdexam.payload.dto.teacherDto.ChangePasswordRequest;
import com.kshrd.hrdexam.payload.dto.teacherDto.ExamPaper;
import com.kshrd.hrdexam.payload.dto.teacherDto.ExamResponse;
import com.kshrd.hrdexam.payload.dto.teacherDto.TeacherResponse;
import com.kshrd.hrdexam.payload.request.studentRequest.RegisterRequest;
import com.kshrd.hrdexam.payload.request.studentRequest.StudentLoginRequest;
import com.kshrd.hrdexam.payload.request.teacherRequest.ResultRequest;
import com.kshrd.hrdexam.payload.request.teacherRequest.UpdateStudentRequest;
import com.kshrd.hrdexam.security.jwt.JwtUtil;
import com.kshrd.hrdexam.service.studentService.StudentServiceImp;
import com.kshrd.hrdexam.service.teacherService.ExamServiceImp;
import com.kshrd.hrdexam.service.teacherService.TeacherServiceImp;
import com.kshrd.hrdexam.service.userDetailService.MyUserDetailService;
import org.modelmapper.internal.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/student")
public class StudentRestController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private MyUserDetailService userDetailService;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private StudentServiceImp studentServiceImp;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ExamServiceImp examServiceImp;

    @Autowired
    private TeacherServiceImp teacherServiceImp;

    @Autowired
    private ObjectMapper objectMapper;


    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
    Date date = new Date();


    public static boolean validation(String validate, String input){

        boolean check = false;

        switch (validate){
            case "ID" :{
                Pattern pattern = Pattern.compile("[a-zA-Z0-9]{6,}$");
                Matcher matcher = pattern.matcher(input);
                check = matcher.matches();

                return check;
            }
            case "USERNAME" : {
                Pattern pattern = Pattern.compile("[a-zA-Z]+$");
                Matcher matcher = pattern.matcher(input);
                check = matcher.matches();

                return check;
            }
            case "PASSWORD" : {
                Pattern pattern = Pattern.compile("[a-zA-Z0-9]{6,}$");
                Matcher matcher = pattern.matcher(input);
                check = matcher.matches();

                return check;
            }
            case "EMAIL" : {
                Pattern pattern = Pattern.compile("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$");
                Matcher matcher = pattern.matcher(input);
                check = matcher.matches();

                return check;
            }
            case "FULLNAME" : {
                Pattern pattern = Pattern.compile("[a-zA-Z\\s]+$");
                Matcher matcher = pattern.matcher(input);
                check = matcher.matches();

                return check;
            }
            case "ONLY_NUMBER" : {
                Pattern pattern = Pattern.compile("(\\d+(?:\\.\\d+)?)");
                Matcher matcher = pattern.matcher(input);
                check = matcher.matches();

                return check;
            }
            case "NOT_EMPTY" : {
                Pattern pattern = Pattern.compile("^[a-zA-Z0-9 _.?!()*=+{},;:'|@#%^/<>&$-]+$");
                Matcher matcher = pattern.matcher(input);
                check = matcher.matches();

                return check;
            }
        }

        return false;
    }



    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @PostMapping("/register")
    public ResponseEntity<?> registerStudent(@RequestBody RegisterRequest request){

        boolean checkId = false;
        boolean checkClass = false;

        Map<String, Object> response = new HashMap<>();

        List<String> studentList = studentServiceImp.getAllStudentId();
        List<String> studentRequestList = studentServiceImp.getAllStudentRequestId();
        studentList.addAll(studentRequestList);
        List<Classroom> classNameList = studentServiceImp.getAllClassName();

        for (Classroom className : classNameList){
            if (className.getClassName().equals(request.getClassName())){
                checkClass = true;
            }
        }

        for (int i=0; i < studentList.size(); i++){
            if (request.getId().equals(studentList.get(i))){
                response.put("status", "OK");
                response.put("message", "This ID is already exist!");
                response.put("success", false);
                response.put("payload", null);
                checkId = true;
            }
        }

        if (!checkId){
            if (validation("ID", request.getId())){
                if (validation("FULLNAME", request.getFullName())){
                    if (validation("EMAIL", request.getEmail())){
                        if (checkClass){
                            if (validation("PASSWORD", request.getPassword())){
                                if (request.getGender().equals("Male") || request.getGender().equals("Female") || request.getGender().equals("male") || request.getGender().equals("female")) {

                                    Classroom classroom = studentServiceImp.getClassIdByClassName(request.getClassName());

                                    StudentRequest studentRequest = new StudentRequest();

                                    studentRequest.setId(request.getId());
                                    studentRequest.setFullName(request.getFullName());
                                    studentRequest.setEmail(request.getEmail());
                                    studentRequest.setPassword(passwordEncoder.encode(request.getPassword()));
                                    studentRequest.setClassroom(classroom);
                                    studentRequest.setRequestAt(formatter.format(date));
                                    studentRequest.setGender(request.getGender());


                                    if (studentServiceImp.registerRequest(studentRequest)) {
                                        response.put("status", "OK");
                                        response.put("message", "Your account is approving by teacher!");
                                        response.put("success", true);
                                        response.put("payload", request);
                                    } else {
                                        response.put("status", "ERROR");
                                        response.put("message", "Register fail!");
                                        response.put("success", false);
                                        response.put("payload", null);
                                    }

                                }else {
                                    response.put("status", "ERROR");
                                    response.put("message", "Gender must be 'Male' or 'Female'!");
                                    response.put("success", false);
                                    response.put("payload", null);
                                }

                            }else {
                                response.put("status", "ERROR");
                                response.put("message", "Password must be at least 6 character!");
                                response.put("success", false);
                                response.put("payload", null);
                            }
                        }else {
                            response.put("status", "ERROR");
                            response.put("message", "Incorrect class name! it should be (Phnom Penh, Battambang ....)");
                            response.put("success", false);
                            response.put("payload", null);
                        }

                    }else {
                        response.put("status", "ERROR");
                        response.put("message", "Bad email!");
                        response.put("success", false);
                        response.put("payload", null);
                    }
                }else {
                    response.put("status", "ERROR");
                    response.put("message", "Full name must be only character!");
                    response.put("success", false);
                    response.put("payload", null);
                }
            }else {
                response.put("status", "ERROR");
                response.put("message", "ID must be at least 6 character!");
                response.put("success", false);
                response.put("payload", null);
            }
        }


        return ResponseEntity.ok().body(response);
    }




    //========================================================================================//
    //                                 Login Method for student                               //
    //========================================================================================//
    //   In the method parameter have RequestBody as StudentLoginRequest that has 2 fields    //
    //   -String id, String password.                                                         //
    //   + we use  authenticationManager.authenticate(                                        //
    //                    new UsernamePasswordAuthenticationToken(id, password)               //
    //     to verify student login with id and password.                                      //
    //   + we use final UserDetails userDetails = userDetailService                           //
    //                .loadUserByUsername(loginRequest.getId());                              //
    //     to get data of student that login from database as UserDetails                     //                                                            //
    //                                                                                        //
    //****************************************************************************************//

    @PostMapping("/login")
    public ResponseEntity<?> loginStudent(@RequestBody StudentLoginRequest loginRequest) throws Exception {

        Map<String, Object> response = new HashMap<>();

        if (validation("ID", loginRequest.getId())){
            if (validation("PASSWORD", loginRequest.getPassword())){

                boolean isPasswordMatch = false;

                Student student = studentServiceImp.loadStudentById(loginRequest.getId());

                if (student != null){

                    isPasswordMatch = passwordEncoder.matches(loginRequest.getPassword(), student.getPassword());

                    if (isPasswordMatch){
                        try {
                            authenticationManager.authenticate(
                                    new UsernamePasswordAuthenticationToken(loginRequest.getId(), loginRequest.getPassword()));
                        } catch (BadCredentialsException e) {
                            throw new Exception("Incorrect username and password!", e);
                        }

                        final UserDetails userDetails = userDetailService
                                .loadUserByUsername(loginRequest.getId());
                        final String jwt = jwtUtil.generateToken(userDetails);


                        StudentLoginDetail studentDto = new StudentLoginDetail();

                        studentDto.setId(student.getId());
                        studentDto.setFullName(student.getFullName());
                        studentDto.setGeneration(student.getGeneration().getGeneration());
                        studentDto.setGender(student.getGender());
                        studentDto.setClassName(student.getClassroom().getClassName());
                        studentDto.setClassId(student.getClassroom().getId());
                        studentDto.setEmail(student.getEmail());
                        studentDto.setToken(jwt);

                        response.put("status", "OK");
                        response.put("message", "You have login successfully!");
                        response.put("success", true);
                        response.put("payload", studentDto);
                    }else {
                        response.put("status", "password");
                        response.put("message", "Incorrect password!");
                        response.put("success", false);
                        response.put("payload", null);
                    }
                }else {
                    response.put("status", "id");
                    response.put("message", "Incorrect ID!");
                    response.put("success", false);
                    response.put("payload", null);
                }


            }else {
                response.put("status", "ERROR");
                response.put("message", "Password must be at least 6 character!");
                response.put("success", false);
                response.put("payload", null);
            }
        }else {
            response.put("status", "ERROR");
            response.put("message", "ID must be at least 6 character!");
            response.put("success", false);
            response.put("payload", null);
        }

        return ResponseEntity.ok(response);
    }





    @GetMapping("/getAssignedExamByStuId/{id}")
    public ResponseEntity<?> getAssignedExamByStuId(@PathVariable String id){

        Map<String, Object> response = new HashMap<>();

        String examId = examServiceImp.getExamIdByStuId(id);

        if (examId != null){

            Exam exam = examServiceImp.getExamById(examId);

            ExamResponse examResponse = new ExamResponse();

            examResponse.setId(exam.getId());
            examResponse.setExamSubject(exam.getExamSubject());
            examResponse.setExamDate(exam.getExamDate());
            examResponse.setExamName(exam.getExamName());
            examResponse.setTotalScore(exam.getTotalScore());
            examResponse.setTeacher(exam.getTeacher().getFullName());
            examResponse.setDuration(exam.getDuration());
            examResponse.setGeneration(exam.getGeneration().getGeneration());
            examResponse.setStatus(exam.getStatus());
            examResponse.setAssignType(exam.getAssignType());

            response.put("status", "OK");
            response.put("message", "Get exam by student ID success!");
            response.put("success", true);
            response.put("payload", examResponse);
        }else {
            response.put("status", "OK");
            response.put("message", "No exam available!");
            response.put("success", false);
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }




    @GetMapping("/getAssignedExamByClassId/{id}")
    public ResponseEntity<?> getAssignedExamByClassId(@PathVariable int id){

        Map<String, Object> response = new HashMap<>();

        String examId = examServiceImp.getExamIdByClassId(id);

        if (examId != null){

            Exam exam = examServiceImp.getExamById(examId);

            ExamResponse examResponse = new ExamResponse();

            examResponse.setId(exam.getId());
            examResponse.setExamSubject(exam.getExamSubject());
            examResponse.setExamDate(exam.getExamDate());
            examResponse.setExamName(exam.getExamName());
            examResponse.setTotalScore(exam.getTotalScore());
            examResponse.setTeacher(exam.getTeacher().getFullName());
            examResponse.setDuration(exam.getDuration());
            examResponse.setGeneration(exam.getGeneration().getGeneration());
            examResponse.setStatus(exam.getStatus());
            examResponse.setAssignType(exam.getAssignType());

            response.put("status", "OK");
            response.put("message", "Get exam by class ID success!");
            response.put("success", true);
            response.put("payload", examResponse);
        }else {
            response.put("status", "OK");
            response.put("message", "No exam available!");
            response.put("success", false);
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }




    @GetMapping("/getExamHistoryByStuId/{id}")
    public ResponseEntity<?> getExamHistoryByStuId(@PathVariable String id){

        Map<String, Object> response = new HashMap<>();

        List<ResultDto> resultHistoryList = studentServiceImp.getExamHistoryByStuId(id);

        List<ResultDto> resultList = new ArrayList<>();

        if (!resultHistoryList.isEmpty()){

            for (ResultDto resultDto : resultHistoryList){
                int status = studentServiceImp.getExamStatusById(resultDto.getExam().getId());
                if (status == 3){
                    resultList.add(resultDto);
                }
            }

            if (resultList.isEmpty()){
                response.put("status", "OK");
                response.put("message", "No exam history!");
                response.put("success", false);
                response.put("payload", null);
            }else {
                response.put("status", "OK");
                response.put("message", "Get exam history success!");
                response.put("success", true);
                response.put("payload", resultList);
            }

        }else {
            response.put("status", "OK");
            response.put("message", "No exam history!");
            response.put("success", false);
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }




    @PostMapping("/submit")
    public ResponseEntity<?> submit(@RequestBody ResultRequest resultRequest){

        Map<String, Object> response = new HashMap<>();

        try{
            Result result = new Result();

            result.setId(RandomString.make(20));
            result.setStudentId(TeacherRestController.getCurrentUser());
            result.setAnswer(resultRequest.getAnswer());
            result.setScore(0);
            result.setStatus(0);

            Map<String, Object> results = new HashMap<>();

            ExamPaper examPaper = objectMapper.readValue(result.getAnswer().toString(), ExamPaper.class);

            results.put("results", examPaper);

            result.setAnswer(results);

            if (examServiceImp.insertResult(resultRequest.getExamId(), result)){

                response.put("status", "OK");
                response.put("message", "Submitted successfully!");
                response.put("success", true);
                response.put("payload", resultRequest);
            }else {
                response.put("status", "OK");
                response.put("message", "Submit fail!");
                response.put("success", false);
                response.put("payload", null);
            }

        }catch (JsonProcessingException e){
            e.printStackTrace();
        }




        return ResponseEntity.ok().body(response);
    }



    @GetMapping("/getAllClassName")
    public ResponseEntity<?> getAllClassName(){

        Map<String, Object> response = new HashMap<>();

        List<Classroom> classList = studentServiceImp.getAllClassName();

        if (!classList.isEmpty()){

            response.put("status", "OK");
            response.put("message", "Get all class name success!");
            response.put("success", true);
            response.put("payload", classList);
        }else {
            response.put("status", "OK");
            response.put("message", "No class yet!");
            response.put("success", false);
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }



    @PatchMapping("/updateProfileById/{id}")
    public ResponseEntity<?> updateProfileById(@PathVariable String id,@RequestBody StudentUpdateRequest student){

        boolean check = false;

        Map<String, Object> response = new HashMap<>();

        if (StudentRestController.validation("ID", id)){
            if (StudentRestController.validation("ID", student.getNewId())){
                if (StudentRestController.validation("FULLNAME", student.getFullName())){
                    if (StudentRestController.validation("EMAIL", student.getEmail())){

                        List<String> studentIdList = studentServiceImp.getAllStudentId();

                        for (String stuId : studentIdList){
                            if(student.getNewId().equals(stuId)){
                                check = true;
                            }
                        }

                        if (!check){
                            if (studentServiceImp.updateProfileById(student, id)){

                                Student studentInfo = teacherServiceImp.getStudentById(student.getNewId());
                                StudentResponse studentResponse = new StudentResponse();

                                studentResponse.setId(studentInfo.getId());
                                studentResponse.setFullName(studentInfo.getFullName());
                                studentResponse.setClassroom(studentInfo.getClassroom().getClassName());
                                studentResponse.setEmail(studentInfo.getEmail());
                                studentResponse.setGender(studentInfo.getGender());
                                studentResponse.setGeneration(studentInfo.getGeneration().getGeneration());
                                studentResponse.setEmail(studentInfo.getEmail());
                                studentResponse.setProfileImg(studentInfo.getProfileImg());
                                studentResponse.setCreateAt(studentInfo.getCreateAt());


                                response.put("status", "OK");
                                response.put("message", "Please login again with your new ID!");
                                response.put("success", true);
                                response.put("payload", studentResponse);
                            }else {
                                response.put("status", "OK");
                                response.put("message", "Student's ID incorrect!");
                                response.put("success", false);
                                response.put("payload", null);
                            }
                        }else {

                            if (student.getNewId().equals(id)){
                                if (studentServiceImp.updateProfileById(student, id)){

                                    Student studentInfo = teacherServiceImp.getStudentById(student.getNewId());
                                    StudentResponse studentResponse = new StudentResponse();

                                    studentResponse.setId(studentInfo.getId());
                                    studentResponse.setFullName(studentInfo.getFullName());
                                    studentResponse.setClassroom(studentInfo.getClassroom().getClassName());
                                    studentResponse.setEmail(studentInfo.getEmail());
                                    studentResponse.setGender(studentInfo.getGender());
                                    studentResponse.setGeneration(studentInfo.getGeneration().getGeneration());
                                    studentResponse.setEmail(studentInfo.getEmail());
                                    studentResponse.setProfileImg(studentInfo.getProfileImg());
                                    studentResponse.setCreateAt(studentInfo.getCreateAt());


                                    response.put("status", "OK");
                                    response.put("message", "Student has been updated successfully!");
                                    response.put("success", true);
                                    response.put("payload", studentResponse);
                                }else {
                                    response.put("status", "OK");
                                    response.put("message", "Student's ID incorrect!");
                                    response.put("success", false);
                                    response.put("payload", null);
                                }
                            }else {
                                response.put("status", "OK");
                                response.put("message", "This ID is already exist!");
                                response.put("success", false);
                                response.put("payload", null);
                            }
                        }

                    }else {
                        response.put("status", "ERROR");
                        response.put("message", "Bad email!");
                        response.put("success", false);
                        response.put("payload", null);
                    }

                }else {
                    response.put("status", "ERROR");
                    response.put("message", "Full name must be only character!");
                    response.put("success", false);
                    response.put("payload", null);
                }

            }else {
                response.put("status", "OK");
                response.put("message", "ID must be at least 6 characters!");
                response.put("success", false);
                response.put("payload", null);
            }

        }else {
            response.put("status", "OK");
            response.put("message", "ID must be at least 6 characters!");
            response.put("success", false);
            response.put("payload", null);
        }

        return ResponseEntity.ok().body(response);
    }



    @PatchMapping("/changePassword/{id}")
    public ResponseEntity<?> changePassword(@PathVariable String id, @RequestBody ChangePasswordRequest passwordRequest) {

        Map<String, Object> response = new HashMap<>();

        boolean isPasswordMatch;

        if (validation("ID", id)){
            if (validation("PASSWORD", passwordRequest.getOldPassword())){
                if (validation("PASSWORD", passwordRequest.getNewPassword())){

                    if (passwordRequest.getNewPassword().equals(passwordRequest.getOldPassword())){
                        response.put("status", "OK");
                        response.put("message", "New password and current password is the same!");
                        response.put("success", false);
                        response.put("payload", passwordRequest);
                    }else {

                        String oldPassword = studentServiceImp.getOldPassword(id);

                        if (oldPassword == null){
                            response.put("status", "OK");
                            response.put("message", "Incorrect ID!");
                            response.put("success", false);
                            response.put("payload", null);
                        }else {

                            isPasswordMatch = passwordEncoder.matches(passwordRequest.getOldPassword(), oldPassword);

                            if (isPasswordMatch){

                                if (studentServiceImp.changePassword(id, passwordEncoder.encode(passwordRequest.getNewPassword()))){

                                    response.put("status", "OK");
                                    response.put("message", "Password changed successfully!");
                                    response.put("success", true);
                                    response.put("payload", passwordRequest.getNewPassword());

                                }else {
                                    response.put("status", "OK");
                                    response.put("message", "Change password fail!");
                                    response.put("success", false);
                                    response.put("payload", null);
                                }

                            }else {
                                response.put("status", "OK");
                                response.put("message", "Incorrect current password!");
                                response.put("success", false);
                                response.put("payload", null);
                            }
                        }
                    }
                }else {
                    response.put("status", "ERROR");
                    response.put("message", "New password must be at least 6 character!");
                    response.put("success", false);
                    response.put("payload", null);
                }
            } else {
                response.put("status", "ERROR");
                response.put("message", "Current password must be at least 6 character!");
                response.put("success", false);
                response.put("payload", null);
            }
        } else {
            response.put("status", "OK");
            response.put("message", "ID must be at least 6 characters!");
            response.put("success", false);
            response.put("payload", null);
        }

        return ResponseEntity.ok().body(response);
    }


    @GetMapping("/getProfile")
    public ResponseEntity<?> getProfile() {

        Map<String, Object> response = new HashMap<>();

        Student studentInfo = studentServiceImp.loadStudentById(TeacherRestController.getCurrentUser());

        if (studentInfo != null) {

            StudentResponse studentResponse = new StudentResponse();

            studentResponse.setId(studentInfo.getId());
            studentResponse.setFullName(studentInfo.getFullName());
            studentResponse.setClassroom(studentInfo.getClassroom().getClassName());
            studentResponse.setEmail(studentInfo.getEmail());
            studentResponse.setGender(studentInfo.getGender());
            studentResponse.setGeneration(studentInfo.getGeneration().getGeneration());
            studentResponse.setEmail(studentInfo.getEmail());
            studentResponse.setProfileImg(studentInfo.getProfileImg());
            studentResponse.setCreateAt(studentInfo.getCreateAt());

            response.put("status", "OK");
            response.put("message", "Get profile success!");
            response.put("success", true);
            response.put("payload", studentResponse);
        } else {
            response.put("status", "OK");
            response.put("message", "Get profile fail");
            response.put("payload", null);
        }
        return ResponseEntity.ok().body(response);
    }



    @GetMapping("/joinExam/{examId}")
    public ResponseEntity<?> joinExam(@PathVariable String examId){

        String stuId = TeacherRestController.getCurrentUser();

        Map<String, Object> response = new HashMap<>();

//        List<Exam> examList = studentServiceImp.getActiveExams();
//
//        if (!examList.isEmpty()){
//
//            String examId = null;
//
//            List<AssignAll> assignAllList = examServiceImp.getIdAssignedStudent();
//            List<AssignClass> getAssignedClassList = examServiceImp.getAssignedClass();
//
//            for (int i=0; i<examList.size(); i++){
//
//            List<Result> resultList = studentServiceImp.checkResultBeforeJoinExam(stuId);
//
//                if (!resultList.isEmpty()){
//
//                    if (!assignAllList.isEmpty()){
//                        for (AssignAll exam : assignAllList){
//                            if (exam.getExamId().equals(examList.get(i).getId()) && stuId.equals(exam.getStudentId())){
//
//
//                                for (Result result : resultList){
//                                    if (result.getExamId().equals(exam.getExamId()) && stuId.equals(result.getStudentId())){
//
//                                        checkExamAvailable = false;
//
//                                    }else {
//
//                                        examId = exam.getExamId();
//                                        checkExamAvailable = true;
//
//                                    }
//                                }
//                            }
//                        }
//                    }
//
//                    if (!getAssignedClassList.isEmpty()){
//                        for (AssignClass exam : getAssignedClassList){
//                            if (exam.getExamId().equals(examList.get(i).getId()) && classId == exam.getClassId()){
//
//
//                                for (Result result : resultList){
//                                    if (result.getExamId().equals(exam.getExamId()) && stuId.equals(result.getStudentId())){
//
//                                        checkExamAvailable = false;
//
//                                    }else {
//
//                                        examId = exam.getExamId();
//                                        checkExamAvailable = true;
//
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }else {
//                    checkExamAvailable = true;
//                }
//            }
//
//            if (checkExamAvailable){
//                response.put("status", "OK");
//                response.put("message", "Get exam success!");
//                response.put("success", true);
//                response.put("payload", examId);
//            }else {
//                response.put("status", "OK");
//                response.put("message", "You can not join exam twice!");
//                response.put("success", false);
//                response.put("payload", null);
//            }
//
//
//        }else {
//            response.put("status", "OK");
//            response.put("message", "No exam yet!");
//            response.put("success", false);
//            response.put("payload", null);
//        }

        List<Result> resultList = studentServiceImp.checkResultBeforeJoinExam(stuId);

        if (!resultList.isEmpty()){
            for (Result result : resultList){
                if (result.getExamId().equals(examId)){

                    response.put("status", "OK");
                    response.put("message", "You can't join exam twice!");
                    response.put("success", false);
                    response.put("payload", false);

                }else {
                    response.put("status", "OK");
                    response.put("message", "You can join exam now!");
                    response.put("success", true);
                    response.put("payload", true);
                }
            }
        }else {
            response.put("status", "OK");
            response.put("message", "You can join exam now!");
            response.put("success", true);
            response.put("payload", true);
        }




        return ResponseEntity.ok().body(response);
    }


    @GetMapping("/getExam")
    public ResponseEntity<?> getExam(){

        String stuId = TeacherRestController.getCurrentUser();
        int classId = studentServiceImp.getClassIdByStuId(stuId);

        Map<String, Object> response = new HashMap<>();

        ExamPaper examPaper = studentServiceImp.getExam();

        boolean isExamAvailable = false;

        if (examPaper != null) {

            List<AssignAll> studentAssignedList = studentServiceImp.getAssignedExamForStudent(stuId);
            List<AssignClass> classAssignedList = studentServiceImp.getAssignedExamForClass(classId);

            for (AssignAll assignAll : studentAssignedList) {
                if (assignAll.getExamId().equals(examPaper.getId()) && assignAll.getStudentId().equals(stuId)) {
                    isExamAvailable = true;
                }
            }


            for (AssignClass assignClass : classAssignedList) {
                if (assignClass.getExamId().equals(examPaper.getId()) && assignClass.getClassId() == classId) {
                    isExamAvailable = true;
                }
            }

            if (isExamAvailable) {
                response.put("status", "OK");
                response.put("message", "Get exam successfully!");
                response.put("success", true);
                response.put("payload", examPaper);
            } else {
                response.put("status", "OK");
                response.put("message", "You don't have exam!");
                response.put("success", false);
                response.put("payload", null);
            }
        }else {
            response.put("status", "OK");
            response.put("message", "You don't have exam!");
            response.put("success", false);
            response.put("payload", null);
        }

        return ResponseEntity.ok().body(response);
    }

}
