package com.kshrd.hrdexam.controller.fileupload;


import com.kshrd.hrdexam.model.FileUpload;
import com.kshrd.hrdexam.service.fileUploadService.FileUploadServiceImp;
import org.modelmapper.internal.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;


@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/v1/file")
public class FIleUploadRestController {

    @Value("${file.upload-dir}")
    String serverPath;

    @Value("${url.images}")
    String imageURL;


    @Autowired
    private FileUploadServiceImp fileUploadServiceImp;



    @PostMapping(value = "/uploadFile", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> uploadFile(@RequestPart("file") MultipartFile file){

        FileUpload fileUpload = new FileUpload();

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
        Date date = new Date();

        if (!file.isEmpty()){

            String originalFilename =  file.getOriginalFilename();
            String afterSavedFilename = UUID.randomUUID().toString() +"-"+ originalFilename.substring(originalFilename.lastIndexOf("-")+1);


            Map<String, Object> response = new HashMap<>();
            response.put("status", "OK");
            response.put("message", "file upload success");
            response.put("success", true);


            try{

//                Files.copy(file.getInputStream(), Paths.get(serverPath, afterSavedFilename));


                Path path = Paths.get(serverPath + afterSavedFilename);
                Files.copy(file.getInputStream(), path);

                fileUpload.setFileId(RandomString.make(10));
                fileUpload.setFileURL(imageURL + afterSavedFilename);
                fileUpload.setDateUpload(formatter.format(date));

                fileUploadServiceImp.fileUploadToDatabase(fileUpload); //to database

                response.put("payload", fileUpload);

            }catch (Exception e){
                e.printStackTrace();
            }

            System.out.println(imageURL + afterSavedFilename);
            return ResponseEntity.ok().body(response);

        }else {

            Map<String, Object> response2 = new HashMap<>();
            response2.put("status","BAD");
            response2.put("message", "file upload faile");
            response2.put("success","false");

            return ResponseEntity.badRequest().body(response2);
        }

    }



    @GetMapping("/fetchAllFile")
    public ResponseEntity<?> getAllFile(){

        List<FileUpload> files = fileUploadServiceImp.getAllImageFromDatabase();

        if (!files.isEmpty()){

            Map<String,Object> response = new HashMap<>();

            response.put("status", "OK");
            response.put("message", "file fetch success");
            response.put("success", true);
            response.put("payload",files);

            return ResponseEntity.badRequest().body(response);

        }else {

            Map<String,Object> response = new HashMap<>();

            response.put("status", "Ok");
            response.put("message", "file fetch success");
            response.put("payload",files);
            response.put("success", false);

            return ResponseEntity.badRequest().body(response);
        }
    }



    @GetMapping("/fetchFileBy/{id}")
    public ResponseEntity<?> getFileById(@PathVariable("id") String fileId){

        FileUpload fileUpload =  fileUploadServiceImp.getImageById(fileId);


        Map<String,Object> response = new HashMap<>();

        response.put("status", "OK");
        response.put("message", "file fetch success");
        response.put("success", true);
        response.put("payload",fileUpload);

        return ResponseEntity.ok().body(response);

    }



}
