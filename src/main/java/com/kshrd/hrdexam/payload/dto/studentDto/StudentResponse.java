package com.kshrd.hrdexam.payload.dto.studentDto;


import com.kshrd.hrdexam.model.Classroom;
import com.kshrd.hrdexam.model.Generation;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentResponse {


    String id;
    String fullName;
    String gender;
    String email;
    int generation;
    String profileImg;
    String classroom;
    String createAt;
}
