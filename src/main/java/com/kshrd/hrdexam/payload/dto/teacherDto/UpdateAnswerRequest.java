package com.kshrd.hrdexam.payload.dto.teacherDto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateAnswerRequest {

    String answerId;
    Object answer;

}
