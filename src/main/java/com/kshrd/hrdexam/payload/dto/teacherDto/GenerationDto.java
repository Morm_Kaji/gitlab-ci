package com.kshrd.hrdexam.payload.dto.teacherDto;

import com.kshrd.hrdexam.model.Student;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GenerationDto {

    int id;
    int generation;
    int type;
    List<Student> students;

}
