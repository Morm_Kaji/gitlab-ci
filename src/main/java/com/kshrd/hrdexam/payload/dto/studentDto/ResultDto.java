package com.kshrd.hrdexam.payload.dto.studentDto;

import com.kshrd.hrdexam.model.Exam;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultDto {

    String id;
    Exam exam;
    String studentId;
    Object answer;
    float score;
}
