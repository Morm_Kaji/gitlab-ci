package com.kshrd.hrdexam.payload.dto.teacherDto;

import com.kshrd.hrdexam.model.Section;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExamPaper {

    String id;
    String examSubject;
    String examDate;
    String examName;
    double totalScore;
    String teacher;
    int duration;
    int generation;
    int status;
    int assignType;
    List<SectionResponse> sectionList;
}
