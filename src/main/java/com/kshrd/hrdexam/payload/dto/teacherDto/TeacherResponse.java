package com.kshrd.hrdexam.payload.dto.teacherDto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TeacherResponse {


    String id;
    String fullName;
    String gender;
    String email;
    String username;
    String profileImg;
}
