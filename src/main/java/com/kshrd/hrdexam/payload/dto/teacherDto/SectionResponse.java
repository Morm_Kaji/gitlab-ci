package com.kshrd.hrdexam.payload.dto.teacherDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class SectionResponse implements Comparable{

    String id;
    String heading;
    String examId;
    int section;
    List<QuestionResponse> questionResponses;

    @Override
    public int compareTo(Object sec) {

        int compareSection = ((com.kshrd.hrdexam.model.Section) sec).getSection();
        return this.section-compareSection;
    }

}