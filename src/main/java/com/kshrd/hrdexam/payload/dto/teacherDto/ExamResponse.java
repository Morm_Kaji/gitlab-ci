package com.kshrd.hrdexam.payload.dto.teacherDto;


import com.kshrd.hrdexam.model.Generation;
import com.kshrd.hrdexam.model.Teacher;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExamResponse {


    String id;
    String examSubject;
    String examDate;
    String examName;
    double totalScore;
    String teacher;
    int duration;
    int generation;
    int status;
    int assignType;
}
