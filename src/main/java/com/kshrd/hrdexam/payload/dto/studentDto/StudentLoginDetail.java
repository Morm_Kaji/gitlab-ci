package com.kshrd.hrdexam.payload.dto.studentDto;

import com.kshrd.hrdexam.model.Classroom;
import com.kshrd.hrdexam.model.Generation;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentLoginDetail {

    private String id;
    private String fullName;
    String gender;
    int generation;
    String className;
    int classId;
    private String token;
    private String email;
}
