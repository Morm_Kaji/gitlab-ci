package com.kshrd.hrdexam.payload.dto.teacherDto;


import com.kshrd.hrdexam.model.Answer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionResponse {

    String id;
    String sectionId;
    String questionTypeId;
    String questionContent;
    String image;
    float score;
    float answeredScore;
    Answer answer;
    String studentAnswered;
}
