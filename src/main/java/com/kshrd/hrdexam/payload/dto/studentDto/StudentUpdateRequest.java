package com.kshrd.hrdexam.payload.dto.studentDto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentUpdateRequest {

    String newId;
    String fullName;
    String gender;
    String email;
    String profileImg;

}
