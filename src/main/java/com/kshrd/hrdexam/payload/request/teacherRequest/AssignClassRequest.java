package com.kshrd.hrdexam.payload.request.teacherRequest;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AssignClassRequest {

    String examId;
    List<Integer> classId;
    List<String> studentId;
}
