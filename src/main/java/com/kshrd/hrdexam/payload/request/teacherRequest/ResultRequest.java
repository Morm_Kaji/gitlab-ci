package com.kshrd.hrdexam.payload.request.teacherRequest;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultRequest {

    String examId;
    Object answer;
}
