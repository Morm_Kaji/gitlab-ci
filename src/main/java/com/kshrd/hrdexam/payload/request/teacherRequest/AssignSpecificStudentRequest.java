package com.kshrd.hrdexam.payload.request.teacherRequest;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AssignSpecificStudentRequest {

    String examId;
    List<String> studentIdList;
}
