package com.kshrd.hrdexam.payload.request.teacherRequest;


import com.kshrd.hrdexam.model.Student;
import com.kshrd.hrdexam.payload.dto.studentDto.StudentResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultResponse {

    String id;
    String examId;
    StudentResponse student;
    Object answer;
    float score;
    int status;
}