package com.kshrd.hrdexam.payload.request.studentRequest;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterRequest {

    String id;
    String fullName;
    String email;
    String password;
    String className;
    String gender;
}
