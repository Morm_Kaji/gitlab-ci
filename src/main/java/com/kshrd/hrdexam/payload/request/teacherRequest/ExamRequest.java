package com.kshrd.hrdexam.payload.request.teacherRequest;


import com.kshrd.hrdexam.model.AssignType;
import com.kshrd.hrdexam.model.Generation;
import com.kshrd.hrdexam.model.Teacher;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExamRequest {

    String examName;
    String examSubject;
    String examDate;
    double totalScore;
    int duration;
    int assignType;
}
