package com.kshrd.hrdexam.payload.request.studentRequest;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JoinExamRequest {

    String studentId;
    int classId;
}
