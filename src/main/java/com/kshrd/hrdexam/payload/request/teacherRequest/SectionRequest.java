package com.kshrd.hrdexam.payload.request.teacherRequest;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SectionRequest {

    int section;
    String heading;
    String examId;
}
