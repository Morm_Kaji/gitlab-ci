package com.kshrd.hrdexam.payload.request.teacherRequest;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionRequest {

    String id;
    String sectionId;
    String questionTypeId;
    String questionContent;
    String image;
    float score;
}
