package com.kshrd.hrdexam.repository.studentRepository;


import com.kshrd.hrdexam.model.*;
import com.kshrd.hrdexam.payload.dto.studentDto.ResultDto;
import com.kshrd.hrdexam.payload.dto.studentDto.StudentUpdateRequest;
import com.kshrd.hrdexam.payload.dto.teacherDto.ExamPaper;
import com.kshrd.hrdexam.payload.dto.teacherDto.QuestionResponse;
import com.kshrd.hrdexam.payload.dto.teacherDto.SectionResponse;
import com.kshrd.hrdexam.payload.request.teacherRequest.UpdateStudentRequest;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.annotations.Result;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository {

    @Insert("INSERT INTO student_request (student_id, full_name, email, password, class_id, request_at, gender) values(#{request.id},#{request.fullName},#{request.email},#{request.password},#{request.classroom.id},#{request.requestAt},#{request.gender})")
    public Boolean registerRequest(@Param("request") StudentRequest request);

    @Select("SELECT *FROM class WHERE class_name=#{name}")
    public Classroom getClassIdByClassName(String name);

    @Select("select *from students where id = #{stuId}")
    @Result(property = "fullName", column = "full_name")
    @Result(property = "classroom", column = "class_id", one = @One(select = "getClassNameById"))
    @Result(property = "profileImg", column = "profile_image")
    @Result(property = "generation", column = "generation_id", one = @One(select = "getGenerationName"))
    @Result(property = "createAt", column = "create_at")
    Student loadStudentById(String stuId);

    @Select("SELECT *FROM class WHERE id=#{class_id}")
    @Result(property = "className", column = "class_name")
    Classroom getClassNameById(@Param("class_id") int id);


    @Select("SELECT *FROM generations WHERE id=#{generation_id}")
    Generation getGenerationName(@Param("generation_id") int id);


    @Select("SELECT *FROM students WHERE email = #{email}")
    public Student findByEmail(@Param("email") String email);

    @Select("SELECT *FROM students WHERE reset_password_token = #{token}")
    public Student findByResetPasswordToken(String token);

    @Update("update students set password=#{student.password}, reset_password_token = #{student.resetPasswordToken}  where id=#{userId}")
    Boolean save(Student student, String userId);

    @Select("SELECT *FROM results WHERE student_id = #{id} AND status='2'")
    @Results({
            @Result(property = "exam", column = "exam_id", one = @One(select = "getExamById")),
            @Result(property = "studentId", column = "student_id"),
            @Result(property = "answer", column = "answer_contents")
    })
    List<ResultDto> getExamHistoryByStuId(@Param("id") String stuId);


    @Select("SELECT *FROM exams WHERE id=#{exam_id}")
    @Results({
            @Result(property = "examSubject", column = "exam_subject"),
            @Result(property = "examName", column = "exam_name"),
            @Result(property = "totalScore", column = "total_score"),
            @Result(property = "examDate", column = "exam_date"),
            @Result(property = "teacher", column = "teacher_id", one = @One(select = "getTeacherById")),
            @Result(property = "generation", column = "generation_id", one = @One(select = "getGenerationNameById"))
    })
    Exam getExamById(@Param("exam_id") String id);

    @Select("SELECT *FROM generations WHERE id=#{generation_id}")
    Generation getGenerationNameById(@Param("generation_id") int id);


    @Select("SELECT *FROM teachers WHERE id=#{teacher_id}")
    @Result(property = "fullName", column = "full_name")
    @Result(property = "profileImg", column = "profile_image")
    Teacher getTeacherById(@Param("teacher_id") String id);


    @Select("SELECT id FROM students")
    List<String> getAllStudentId();


    @Select("SELECT student_id FROM student_request")
    List<String> getAllStudentRequestId();


    @Select("SELECT *FROM class")
    @Result(property = "className", column = "class_name")
    List<Classroom> getAllClassName();



    @Update("UPDATE students SET id=#{request.newId}, full_name=#{request.fullName}, email=#{request.email}, gender=#{request.gender}, profile_image=#{request.profileImg} WHERE id=#{stuId}")
    Boolean updateProfileById(@Param("request") StudentUpdateRequest request, @Param("stuId") String stuId);


    @Update("UPDATE students SET id=#{newId} WHERE id=#{oldId}")
    Boolean updateStudentIdById(@Param("oldId") String oldId, @Param("newId") String newId);

    @Update("UPDATE students SET password=#{newPassword} WHERE id = #{id}")
    Boolean changePassword(@Param("id") String id, @Param("newPassword") String newPassword);

    @Select("SELECT password FROM students WHERE id = #{id}")
    String getOldPassword(@Param("id") String id);


    @Select("SELECT *FROM exams WHERE status='1'")
    @Results({
            @Result(property = "examSubject", column = "exam_subject"),
            @Result(property = "examName", column = "exam_name"),
            @Result(property = "totalScore", column = "total_score"),
            @Result(property = "examDate", column = "exam_date"),
            @Result(property = "teacher", column = "teacher_id", one = @One(select = "getTeacherById")),
            @Result(property = "generation", column = "generation_id", one = @One(select = "getGenerationNameById"))
    })
    List<Exam> getActiveExams();


    @Select("SELECT class_id FROM students WHERE id = #{stuId}")
    int getClassIdByStuId(@Param("stuId") String stuId);



    @Select("SELECT *FROM results WHERE student_id = #{stuId}")
    @Results({
            @Result(property = "examId", column = "exam_id"),
            @Result(property = "studentId", column = "student_id"),
            @Result(property = "answer", column = "answer_contents")
    })
    List<com.kshrd.hrdexam.model.Result> checkResultBeforeJoinExam(@Param("stuId") String stuId);

    @Select("SELECT *FROM exams WHERE status='1'")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "examSubject", column = "exam_subject"),
            @Result(property = "examName", column = "exam_name"),
            @Result(property = "totalScore", column = "total_score"),
            @Result(property = "examDate", column = "exam_date"),
            @Result(property = "teacher", column = "teacher_id", one = @One(select = "getTeacherNameById")),
            @Result(property = "generation", column = "generation_id", one = @One(select = "getGenNameById")),
            @Result(property = "sectionList", column = "id", many = @Many(select = "getAllSectionByExamId"))
    })
    ExamPaper getExam();

    @Select("SELECT *FROM sessions WHERE exam_id=#{id} ORDER BY section")
    @Result(property = "id", column = "id")
    @Result(property = "examId", column = "exam_id")
    @Result(property = "questionResponses", column = "id", many = @Many(select = "getAllQuestionBySectionId"))
    List<SectionResponse> getAllSectionByExamId(@Param("id") String examId);


    @Select("SELECT *FROM questions WHERE section_id=#{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "sectionId", column = "section_id"),
            @Result(property = "questionTypeId", column = "question_type_id"),
            @Result(property = "questionContent", column = "question_contents"),
            @Result(property = "answer", column = "id", one = @One(select = "getAnswerByQuestionId")),
    })
    List<QuestionResponse> getAllQuestionBySectionId(@Param("id") String id);

    @Select("SELECT *FROM answer WHERE question_id=#{id}")
    @Result(property = "answer", column = "answer")
    @Result(property = "questionId", column = "question_id")
    Answer getAnswerByQuestionId(@Param("id") String questionId);


    @Select("SELECT full_name FROM teachers WHERE id=#{teacher_id}")
    @Result(property = "fullName", column = "full_name")
    String getTeacherNameById(@Param("teacher_id") String id);


    @Select("SELECT generation FROM generations WHERE id=#{generation_id}")
    int getGenNameById(@Param("generation_id") int id);


    @Select("SELECT *FROM specific_student WHERE student_id=#{id}")
    @Result(property = "examId", column = "exam_id")
    @Result(property = "studentId", column = "student_id")
    List<AssignAll> getAssignedExamForStudent(@Param("id") String id);


    @Select("SELECT *FROM specific_class WHERE class_id=#{id}")
    @Result(property = "examId", column = "exam_id")
    @Result(property = "classId", column = "class_id")
    List<AssignClass> getAssignedExamForClass(@Param("id") int id);


    @Select("SELECT status FROM exams WHERE id=#{examId}")
    int getExamStatusById(@Param("examId") String id);
}
