package com.kshrd.hrdexam.repository.teacherRepository;


import com.kshrd.hrdexam.model.*;
import com.kshrd.hrdexam.payload.dto.studentDto.StudentResponse;
import com.kshrd.hrdexam.payload.dto.teacherDto.*;
import com.kshrd.hrdexam.payload.request.teacherRequest.*;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.annotations.Result;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExamRepository {


    @Insert("INSERT INTO exams (" +
            "id, exam_subject, exam_date, exam_name, total_score, teacher_id, duration, generation_id, status, assign_type_id) " +
            "VALUES (#{exam.id}, #{exam.examSubject}, #{exam.examDate}, #{exam.examName}, #{exam.totalScore}, #{exam.teacher.id}," +
            " #{exam.duration}, #{exam.generation.id}, #{exam.status}, #{exam.assignType})")
    Boolean createExam(@Param("exam")Exam exam);



    @Select("SELECT *FROM exams WHERE status='1' OR status='0'")
    @Results({
            @Result(property = "examSubject", column = "exam_subject"),
            @Result(property = "examName", column = "exam_name"),
            @Result(property = "totalScore", column = "total_score"),
            @Result(property = "examDate", column = "exam_date"),
            @Result(property = "teacher", column = "teacher_id", one = @One(select = "getTeacherById")),
            @Result(property = "generation", column = "generation_id", one = @One(select = "getGenerationNameById"))
    })
    List<Exam> getAllUnassignedExam();


    @Select("SELECT *FROM generations WHERE id=#{generation_id}")
    Generation getGenerationNameById(@Param("generation_id") int id);


    @Select("SELECT *FROM teachers WHERE id=#{teacher_id}")
    @Result(property = "fullName", column = "full_name")
    @Result(property = "profileImg", column = "profile_image")
    Teacher getTeacherById(@Param("teacher_id") String id);


    @Select("SELECT *FROM exams WHERE status='2' AND generation_id=#{id}")
    @Results({
            @Result(property = "examSubject", column = "exam_subject"),
            @Result(property = "examName", column = "exam_name"),
            @Result(property = "totalScore", column = "total_score"),
            @Result(property = "examDate", column = "exam_date"),
            @Result(property = "teacher", column = "teacher_id", one = @One(select = "getTeacherById")),
            @Result(property = "generation", column = "generation_id", one = @One(select = "getGenerationNameById"))
    })
    List<Exam> getAllAssignedExamByGenId(@Param("id") int id);


    @Select("SELECT *FROM generations WHERE generation = #{generation} AND type='1'")
    @Result(property = "examList",column = "id", many = @Many(select = "getAllAssignedExamByGenId"))
    ExamGenerationDto getAllAssignedExamByGen(@Param("generation") int generation);


    @Select("SELECT *FROM generations WHERE generation = #{generation} AND type='2'")
    @Result(property = "examList",column = "id", many = @Many(select = "getAllAssignedExamByGenId"))
    ExamGenerationDto getAllAdvanceAssignedExamByGen(@Param("generation") int generation);


    @Insert("INSERT INTO sessions (id, heading, exam_id, section) VALUES(#{section.id},#{section.heading},#{section.examId}, #{section.section})")
    Boolean createSection(@Param("section") Section section);


    @Select("SELECT *FROM sessions WHERE exam_id=#{examId}")
    @Result(property = "examId", column = "exam_id")
    List<Section> getSectionByExamId(@Param("examId") String examId);


    @Update("UPDATE exams SET exam_subject=#{request.examSubject}, exam_name=#{request.examName}, exam_date=#{request.examDate}, total_score=#{request.totalScore}, duration=#{request.duration}, assign_type_id=#{request.assignType} WHERE id=#{id}")
    Boolean updateExam(@Param("id") String id, @Param("request") ExamRequest examRequest);



    @Update("UPDATE sessions SET heading=#{request.heading}, exam_id=#{request.examId}, section=#{request.section} WHERE id=#{id}")
    Boolean updateSection(@Param("id") String id, @Param("request")Section section);


    @Delete("DELETE FROM exams WHERE id=#{id}")
    Boolean deleteExamById(@Param("id") String id);


    @Select("SELECT *FROM exams WHERE id=#{id}")
    @Results({
            @Result(property = "examSubject", column = "exam_subject"),
            @Result(property = "examName", column = "exam_name"),
            @Result(property = "totalScore", column = "total_score"),
            @Result(property = "examDate", column = "exam_date"),
            @Result(property = "teacher", column = "teacher_id", one = @One(select = "getTeacherById")),
            @Result(property = "generation", column = "generation_id", one = @One(select = "getGenerationNameById"))
    })
    Exam getExamById(@Param("id") String id);


    @Delete("DELETE FROM sessions WHERE id=#{id}")
    Boolean deleteSectionById(@Param("id") String id);


    @Select("SELECT *FROM sessions WHERE id=#{id}")
    @Result(property = "examId", column = "exam_id")
    Section getSectionById(@Param("id") String id);


    @Insert("INSERT INTO questions (id, section_id, question_type_id, question_contents, image, score) VALUES " +
            "(#{question.id},#{question.sectionId}, #{question.questionTypeId}, #{question.questionContent}, #{question.image}, #{question.score} )")
    Boolean createQuestion(@Param("question")Question question);


    @Select("SELECT *FROM questions WHERE section_id=#{id}")
    @Results({
            @Result(property = "sectionId", column = "section_id"),
            @Result(property = "questionTypeId", column = "question_type_id"),
            @Result(property = "questionContent", column = "question_contents"),
    })
    List<Question> getQuestionBySectionId(@Param("id") String id);



    @Select("SELECT *FROM questions WHERE id=#{id}")
    @Results({
            @Result(property = "sectionId", column = "section_id"),
            @Result(property = "questionTypeId", column = "question_type_id"),
            @Result(property = "questionContent", column = "question_contents"),
    })
    Question getQuestionById(@Param("id") String id);



    @Update("UPDATE questions SET question_type_id=#{request.questionTypeId}, " +
            "question_contents=#{request.questionContent}, image=#{request.image}, " +
            "score=#{request.score} WHERE id=#{id}")
    Boolean updateQuestion(@Param("id") String id, @Param("request") QuestionRequest questionRequest);



    @Delete("DELETE FROM questions WHERE id=#{id}")
    Boolean deleteQuestionById(@Param("id") String id);



    @Insert("INSERT INTO answer (id, question_id, answer) VALUES(#{answer.id, jdbcType=VARCHAR}, #{answer.questionId, jdbcType=VARCHAR}," +
            "#{answer.answer, jdbcType=OTHER, typeHandler=com.kshrd.hrdexam.config.JSONTypeHandler})")
    Boolean createAnswer(@Param("answer")Answer answer);


    @Update("UPDATE exams SET status='2' WHERE id=#{examId}")
    Boolean deactivateExam(@Param("examId") String id);



    @Select("SELECT *FROM generations WHERE status = '1'")
    @Result(property = "id", column = "id")
    @Result(property = "students",column = "id", many = @Many(select = "getStudentIdByGeneration"))
    GenerationDto getAllStudentsByGeneration();


    @Select("SELECT *FROM students WHERE generation_id=#{id}")
    @Result(property = "fullName", column = "full_name")
    @Result(property = "classroom", column = "class_id", one = @One(select = "getClassNameById"))
    @Result(property = "profileImg", column = "profile_image")
    @Result(property = "generation", column = "generation_id", one = @One(select = "getGenerationName"))
    @Result(property = "createAt", column = "create_at")
    List<Student> getStudentIdByGeneration(@Param("id") int id);


    @Select("SELECT *FROM class WHERE id=#{class_id}")
    @Result(property = "className", column = "class_name")
    Classroom getClassNameById(@Param("class_id") int id);


    @Select("SELECT *FROM generations WHERE id=#{generation_id}")
    Generation getGenerationName(@Param("generation_id") int id);


    @Insert("INSERT INTO specific_student (exam_id, student_id) VALUES (#{examId}, #{stuId})")
    Boolean assignAllStudents(@Param("examId") String examId, @Param("stuId") String stuId);


    @Select("SELECT *FROM specific_student")
    @Result(property = "examId", column = "exam_id")
    @Result(property = "studentId", column = "student_id")
    List<AssignAll> getIdAssignedStudent();


    @Select("SELECT *FROM specific_class")
    @Result(property = "examId", column = "exam_id")
    @Result(property = "classId", column = "class_id")
    List<AssignClass> getAssignedClass();


    @Insert("INSERT INTO specific_class (exam_id, class_id) VALUES (#{examId}, #{classId})")
    Boolean assignToClass(@Param("examId") String examId, @Param("classId") int classId);


    @Update("UPDATE exams SET status='1' WHERE id=#{examId}")
    Boolean activateExam(@Param("examId") String id);


    @Select("SELECT class_id FROM students WHERE id=#{stuId}")
    int getClassIdByStuId(@Param("stuId") String stuId);



    @Select("SELECT *FROM specific_student WHERE student_id=#{stuId}")
    String getExamIdByStuId(@Param("stuId") String stuId);


    @Select("SELECT *FROM specific_class WHERE class_id=#{classId}")
    String getExamIdByClassId(@Param("classId") Integer classId);


    @Insert("INSERT INTO results (id, exam_id, student_id, answer_contents, score, status) " +
            "VALUES(#{result.id, jdbcType=VARCHAR}, #{examId, jdbcType=VARCHAR}," +
            "#{result.studentId, jdbcType=VARCHAR},#{result.answer, jdbcType=OTHER, typeHandler=com.kshrd.hrdexam.config.JSONTypeHandler}, #{result.score}, #{result.status})")
    Boolean insertResult(String examId, @Param("result") com.kshrd.hrdexam.model.Result result);


    @Select("SELECT *FROM results WHERE exam_id = #{examId}")
    @Results({
            @Result(property = "examId", column = "exam_id"),
            @Result(property = "student", column = "student_id", one = @One(select = "getStudentById")),
            @Result(property = "answer", column = "answer_contents")
    })
    List<ResultResponse> getSubmittedResultsByExamId(@Param("examId") String examId);


    @Select("select *from students where id = #{stuId}")
    @Result(property = "fullName", column = "full_name")
    @Result(property = "classroom", column = "class_id", one = @One(select = "getClassName"))
    @Result(property = "profileImg", column = "profile_image")
    @Result(property = "generation", column = "generation_id", one = @One(select = "getGeneration"))
    @Result(property = "createAt", column = "create_at")
    StudentResponse getStudentById(String stuId);


    @Select("SELECT class_name FROM class WHERE id=#{class_id}")
    @Result(property = "className", column = "class_name")
    String getClassName(@Param("class_id") int id);


    @Select("SELECT generation FROM generations WHERE id=#{generation_id}")
    int getGeneration(@Param("generation_id") int id);


    @Update("UPDATE results SET score=#{score} WHERE id=#{id}")
    Boolean correctAnswerByResultId(@Param("id") String resultId, @Param("score") float score);


    @Select("SELECT *FROM results WHERE id=#{id}")
    @Results({
            @Result(property = "examId", column = "exam_id"),
            @Result(property = "studentId", column = "student_id"),
            @Result(property = "answer", column = "answer_contents")
    })
    com.kshrd.hrdexam.model.Result viewResultById(@Param("id") String resultId);


    @Update("UPDATE results SET status='1' WHERE id=#{id}")
    Boolean updateResultToChecking(@Param("id") String resultId);


    @Select("SELECT *FROM results WHERE exam_id = #{examId} AND status='1'")
    @Results({
            @Result(property = "examId", column = "exam_id"),
            @Result(property = "studentId", column = "student_id"),
            @Result(property = "answer", column = "answer_contents")
    })
    List<com.kshrd.hrdexam.model.Result> CheckingResultsByExamId(@Param("examId") String examId);


    @Select("SELECT *FROM generations WHERE type='1'")
    List<Generation> getAllBasicGeneration();


    @Select("SELECT *FROM generations WHERE type='2'")
    List<Generation> getAllAdvanceGeneration();


    @Select("SELECT section FROM sessions WHERE exam_id=#{id}")
    List<Integer> getSectionOrderByExamId(@Param("id") String examId);


    @Select("SELECT *FROM exams WHERE id=#{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "examSubject", column = "exam_subject"),
            @Result(property = "examName", column = "exam_name"),
            @Result(property = "totalScore", column = "total_score"),
            @Result(property = "examDate", column = "exam_date"),
            @Result(property = "assignType", column = "assign_type_id"),
            @Result(property = "teacher", column = "teacher_id", one = @One(select = "getTeacherNameById")),
            @Result(property = "generation", column = "generation_id", one = @One(select = "getGenNameById")),
            @Result(property = "sectionList", column = "id", many = @Many(select = "getAllSectionByExamId"))
    })
    ExamPaper getExamPaperById(@Param("id") String id);

    @Select("SELECT *FROM sessions WHERE exam_id=#{id} ORDER BY section")
    @Result(property = "id", column = "id")
    @Result(property = "examId", column = "exam_id")
    @Result(property = "questionResponses", column = "id", many = @Many(select = "getAllQuestionBySectionId"))
    List<SectionResponse> getAllSectionByExamId(@Param("id") String examId);


    @Select("SELECT *FROM questions WHERE section_id=#{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "sectionId", column = "section_id"),
            @Result(property = "questionTypeId", column = "question_type_id"),
            @Result(property = "questionContent", column = "question_contents"),
            @Result(property = "answer", column = "id", one = @One(select = "getAnswerByQuestionId")),
    })
    List<QuestionResponse> getAllQuestionBySectionId(@Param("id") String id);

    @Select("SELECT *FROM answer WHERE question_id=#{id}")
    @Result(property = "answer", column = "answer")
    @Result(property = "questionId", column = "question_id")
    Answer getAnswerByQuestionId(@Param("id") String questionId);


    @Select("SELECT full_name FROM teachers WHERE id=#{teacher_id}")
    @Result(property = "fullName", column = "full_name")
    String getTeacherNameById(@Param("teacher_id") String id);


    @Select("SELECT generation FROM generations WHERE id=#{generation_id}")
    int getGenNameById(@Param("generation_id") int id);


    @Select("SELECT *FROM assign_types")
    @Result(property = "assignType", column = "assign_type")
    List<AssignType> getAllAssignTypes();


    @Update("UPDATE answer SET answer=#{answerRequest.answer, jdbcType=OTHER, typeHandler=com.kshrd.hrdexam.config.JSONTypeHandler} WHERE id=#{answerRequest.answerId}")
    Boolean updateAnswer(@Param("answerRequest") UpdateAnswerRequest answerRequest);


    @Select("SELECT question_type_id FROM questions WHERE id=#{qId}")
    String getQuestionTypeIdByQuestionId(@Param("qId") String id);


    @Select("SELECT question_id FROM answer WHERE id=#{answerId}")
    String getQuestionIdByAnswerId(@Param("answerId") String id);

    @Update("UPDATE questions SET question_type_id=#{questionType} WHERE id=#{questionId}")
    Boolean updateQuestionTypeByQuestionId(@Param("questionId") String questionId,@Param("questionType") String newQuestionType);


    @Delete("DELETE FROM answer WHERE question_id=#{questionId}")
    Boolean deleteAnswerByQuestionId(@Param("questionId") String id);

    @Select("SELECT *FROM exams WHERE status ='1'")
    Exam getExam();


    @Update("UPDATE exams SET assign_type_id=#{newType} WHERE id=#{ExamId}")
    Boolean updateExamAssignTypeId(@Param("ExamId") String ExamId, @Param("newType") int newType);


    @Select("SELECT *FROM exams WHERE status='2' OR status='3'")
    @Results({
            @Result(property = "examSubject", column = "exam_subject"),
            @Result(property = "examName", column = "exam_name"),
            @Result(property = "totalScore", column = "total_score"),
            @Result(property = "examDate", column = "exam_date"),
            @Result(property = "teacher", column = "teacher_id", one = @One(select = "getTeacherById")),
            @Result(property = "generation", column = "generation_id", one = @One(select = "getGenerationNameById"))
    })
    List<Exam> getAllAssignedExam();


    @Update("UPDATE results SET answer_contents=#{resultRequest.result, jdbcType=OTHER, typeHandler=com.kshrd.hrdexam.config.JSONTypeHandler}, status=#{resultRequest.status}, score=#{resultRequest.score} WHERE id=#{resultRequest.resultId}")
    Boolean saveResult(@Param("resultRequest") SaveResultRequest resultRequest);


    @Update("UPDATE exams SET status='3' WHERE id=#{ExamId}")
    Boolean turnInResult(@Param("ExamId") String ExamId);
}
