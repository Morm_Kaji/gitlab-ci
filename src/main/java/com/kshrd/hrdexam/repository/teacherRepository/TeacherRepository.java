package com.kshrd.hrdexam.repository.teacherRepository;


import com.kshrd.hrdexam.model.*;
import com.kshrd.hrdexam.payload.dto.studentDto.StudentResponse;
import com.kshrd.hrdexam.payload.dto.teacherDto.GenerationDto;
import com.kshrd.hrdexam.payload.request.teacherRequest.ProfileUpdateRequest;
import com.kshrd.hrdexam.payload.request.teacherRequest.UpdateStudentRequest;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.annotations.Result;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TeacherRepository {

    @Select("SELECT *FROM student_request")
    @Result(property = "id", column = "student_id")
    @Result(property = "fullName", column = "full_name")
    @Result(property = "requestAt", column = "request_at")
    @Result(property = "classroom", column = "class_id", one = @One(select = "getClassroomById"))
    List<StudentRequest> getAllStudentsRequest();

    @Select("SELECT *FROM student_request WHERE student_id = #{id}")
    @Result(property = "id", column = "student_id")
    @Result(property = "fullName", column = "full_name")
    @Result(property = "requestAt", column = "request_at")
    @Result(property = "classroom", column = "class_id", one = @One(select = "getClassroomById"))
    StudentRequest getReqStuById(String id);

    @Insert("INSERT INTO students (id, full_name, gender, email, password,generation_id, profile_image, class_id, create_at) " +
            "values(#{student.id},#{student.fullName},#{student.gender},#{student.email},#{student.password},#{student.generation.id},#{student.profileImg},#{student.classroom.id},#{student.createAt})")
    public Boolean insertAcceptedStu(@Param("student")Student student);

    @Select("SELECT id FROM generations WHERE status='1'")
    public int getCurrentGenId();

    @Delete("DELETE FROM student_request WHERE student_id=#{id}")
    Boolean deleteAcceptedStuById(String id);

    @Select("select *from teachers where username = #{tcUsername}")
    @Result(property = "fullName", column = "full_name")
    @Result(property = "profileImg", column = "profile_image")
    Teacher loadTeacherByUsername(String tcUsername);


    @Select("SELECT *FROM teachers WHERE email = #{email}")
    public Teacher findByEmail(@Param("email") String email);

    @Select("SELECT *FROM teachers WHERE reset_password_token = #{token}")
    public Teacher findByResetPasswordToken(String token);

    @Update("update teachers set password=#{teacher.password}, reset_password_token = #{teacher.resetPasswordToken}  where id=#{userId}")
    Boolean save(Teacher teacher, String userId);

    @Select("SELECT *FROM generations WHERE generation = #{generation}")
    @Result(property = "id", column = "id")
    @Result(property = "students",column = "id", many = @Many(select = "getGenerationIdByGeneration"))
    GenerationDto getAllStudentsByGeneration(@Param("generation") int generation);


    @Select("SELECT *FROM generations WHERE id = #{id}")
    @Result(property = "id", column = "id")
    @Result(property = "students",column = "id", many = @Many(select = "getGenerationIdByGeneration"))
    GenerationDto getAllBasicStudentsByGeneration(@Param("id") int id);



    @Select("SELECT *FROM generations WHERE generation = #{generation} AND type='2'")
    @Result(property = "id", column = "id")
    @Result(property = "students",column = "id", many = @Many(select = "getGenerationIdByGeneration"))
    GenerationDto getAllAdvanceStudentsByGeneration(@Param("generation") int generation);


    @Select("SELECT id, full_name, gender, email, generation_id, class_id , profile_image, create_at FROM students WHERE generation_id=#{id}")
    @Result(property = "id", column = "id")
    @Result(property = "fullName", column = "full_name")
    @Result(property = "classroom", column = "class_id", one = @One(select = "getClassNameById"))
    @Result(property = "profileImg", column = "profile_image")
    @Result(property = "generation", column = "generation_id", one = @One(select = "getGenerationById"))
    @Result(property = "createAt", column = "create_at")
    List<StudentResponse> getGenerationIdByGeneration(@Param("id") int id);


    @Delete("DELETE FROM students WHERE id=#{id}")
    Boolean deleteStudentById(@Param("id") String id);

    @Select("SELECT *FROM students WHERE id=#{id}")
    @Result(property = "fullName", column = "full_name")
    @Result(property = "profileImg", column = "profile_image")
    @Result(property = "classroom", column = "class_id", one = @One(select = "getClassroomById"))
    @Result(property = "generation", column = "generation_id", one = @One(select = "getGenerationNameById"))
    @Result(property = "createAt", column = "create_at")
    Student getStudentById(@Param("id") String id);


    @Select("SELECT class_name FROM class WHERE id=#{class_id}")
//    @Result(property = "className", column = "class_name")
    String getClassNameById(@Param("class_id") int id);

    @Select("SELECT *FROM class WHERE id=#{class_id}")
    @Result(property = "className", column = "class_name")
    Classroom getClassroomById(@Param("class_id") int id);


    @Select("SELECT generation FROM generations WHERE id=#{generation_id}")
    int getGenerationById(@Param("generation_id") int id);


    @Select("SELECT *FROM generations WHERE id=#{generation_id}")
    Generation getGenerationNameById(@Param("generation_id") int id);



    @Update("UPDATE students SET id=#{request.newId}, full_name=#{request.fullName}, email=#{request.email} WHERE id=#{stuId}")
    Boolean updateStudentById(@Param("request") UpdateStudentRequest request,@Param("stuId") String stuId);



    @Update("UPDATE teachers SET full_name=#{request.fullName}, email =#{request.email}, username=#{request.username}, profile_image=#{request.profileImg}  WHERE username=#{username}")
    Boolean updateProfile(ProfileUpdateRequest request, String username);



    @Select("SELECT id, full_name, gender, email, generation_id, class_id , profile_image, create_at FROM students WHERE class_id=#{id}")
    @Result(property = "id", column = "id")
    @Result(property = "fullName", column = "full_name")
    @Result(property = "classroom", column = "class_id", one = @One(select = "getClassNameById"))
    @Result(property = "profileImg", column = "profile_image")
    @Result(property = "generation", column = "generation_id", one = @One(select = "getGenerationById"))
    @Result(property = "createAt", column = "create_at")
    List<StudentResponse> getAllStudentsByClassId(@Param("id") int id);


    @Update("UPDATE teachers SET password=#{newPassword} WHERE username = #{username}")
    Boolean changePassword(@Param("username") String username, @Param("newPassword") String newPassword);

    @Select("SELECT password FROM teachers WHERE username = #{username}")
    String getOldPassword(@Param("username") String username);


    @Select("SELECT username FROM teachers")
    List<String> getAllTeacherUsername();


    @Select("SELECT *FROM generations ORDER BY generation DESC")
    List<Generation> getAllGenerations();

}
