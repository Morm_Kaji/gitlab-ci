package com.kshrd.hrdexam.repository.fileUploadRepository;


import com.kshrd.hrdexam.model.FileUpload;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface FileUploadRepository {



    @Insert("insert into table_file_upload(id, file_url, create_date)  " +
            "values (#{fileUpload.fileId},#{fileUpload.fileURL},#{fileUpload.dateUpload})")
    boolean fileUploadToDatabase(@Param("fileUpload") FileUpload fileUpload);



    @Select("select * from table_file_upload")
    @Result(property = "fileId", column = "id")
    @Result(property = "fileURL", column = "file_url")
    @Result(property = "dateUpload", column = "create_date")
    List<FileUpload> getAllImageFromDatabase();




    @Select("select * from table_file_upload where id = #{fileId}")
    @Result(property = "fileId", column = "id")
    @Result(property = "fileURL", column = "file_url")
    @Result(property = "dateUpload", column = "create_date")
    FileUpload getImageById(@Param("fileId") String fileId);



}
