package com.kshrd.hrdexam.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Exam {

    String id;
    String examSubject;
    String examDate;
    String examName;
    double totalScore;
    Teacher teacher;
    int duration;
    Generation generation;
    int status;
    int assignType;

}
