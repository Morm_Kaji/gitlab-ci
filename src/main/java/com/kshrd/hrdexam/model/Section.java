package com.kshrd.hrdexam.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Section implements Comparable{

    String id;
    String heading;
    String examId;
    int section;

    @Override
    public int compareTo(Object sec) {

        int compareSection = ((Section) sec).getSection();
        return this.section-compareSection;
    }

}
