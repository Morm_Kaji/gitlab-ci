package com.kshrd.hrdexam.model;


import com.kshrd.hrdexam.payload.dto.teacherDto.GenerationDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student {

    String id;
    String fullName;
    String gender;
    String email;
    String password;
    Generation generation;
    String profileImg;
    Classroom classroom;
    String createAt;
    String resetPasswordToken;
}
