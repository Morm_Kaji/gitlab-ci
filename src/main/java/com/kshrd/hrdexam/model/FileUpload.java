package com.kshrd.hrdexam.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class FileUpload {

    private String fileId;
    private String fileURL;
    private String dateUpload;

}
