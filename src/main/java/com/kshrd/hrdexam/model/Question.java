package com.kshrd.hrdexam.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Question {

    String id;
    String sectionId;
    String questionTypeId;
    String questionContent;
    String image;
    float score;
}
