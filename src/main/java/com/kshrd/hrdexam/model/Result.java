package com.kshrd.hrdexam.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result {

    String id;
    String examId;
    String studentId;
    Object answer;
    float score;
    int status;
}
