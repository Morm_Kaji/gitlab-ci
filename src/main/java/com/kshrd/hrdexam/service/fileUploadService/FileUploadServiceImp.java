package com.kshrd.hrdexam.service.fileUploadService;


import com.kshrd.hrdexam.model.FileUpload;
import com.kshrd.hrdexam.repository.fileUploadRepository.FileUploadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FileUploadServiceImp implements FileUploadService {



    @Autowired
    private FileUploadRepository fileUploadRepository;




    @Override
    public boolean fileUploadToDatabase(FileUpload fileUpload) {
        return fileUploadRepository.fileUploadToDatabase(fileUpload);
    }




    @Override
    public List<FileUpload> getAllImageFromDatabase() {
        return fileUploadRepository.getAllImageFromDatabase();
    }




    @Override
    public FileUpload getImageById(String fileId) {
        return fileUploadRepository.getImageById(fileId);
    }


}
