package com.kshrd.hrdexam.service.fileUploadService;


import com.kshrd.hrdexam.model.FileUpload;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface FileUploadService {

    boolean fileUploadToDatabase(FileUpload fileUpload);

    List<FileUpload> getAllImageFromDatabase();

    FileUpload getImageById(String fileId);
}
