package com.kshrd.hrdexam.service.studentService;

import com.kshrd.hrdexam.model.*;
import com.kshrd.hrdexam.payload.dto.studentDto.ResultDto;
import com.kshrd.hrdexam.payload.dto.studentDto.StudentUpdateRequest;
import com.kshrd.hrdexam.payload.dto.teacherDto.ExamPaper;
import com.kshrd.hrdexam.repository.studentRepository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImp implements StudentService{

    @Autowired
    private StudentRepository studentRepository;

    @Override
    public Boolean registerRequest(StudentRequest request) {
        return studentRepository.registerRequest(request);
    }

    @Override
    public Classroom getClassIdByClassName(String name) {
        return studentRepository.getClassIdByClassName(name);
    }

    @Override
    public Student loadStudentById(String stuId) {
        return studentRepository.loadStudentById(stuId);
    }

    @Override
    public Student findByEmail(String email) {
        return studentRepository.findByEmail(email);
    }

    @Override
    public Student findByResetPasswordToken(String token) {
        return studentRepository.findByResetPasswordToken(token);
    }




    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//
    @Override
    public void updateResetPasswordToken(String token, String email) throws Exception {

        Student student = studentRepository.findByEmail(email);
        if (student != null) {
            student.setResetPasswordToken(token);
            studentRepository.save(student, student.getId());
        } else {
            throw new Exception("Could not find any user with the email " + email);
        }
    }

    @Override
    public Student getByResetPasswordToken(String token) {
        return studentRepository.findByResetPasswordToken(token);
    }



    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @Override
    public void updatePassword(Student student, String newPassword) {

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode(newPassword);
        student.setPassword(encodedPassword);

        student.setResetPasswordToken(null);
        studentRepository.save(student, student.getId());

    }

    @Override
    public List<ResultDto> getExamHistoryByStuId(String stuId) {
        return studentRepository.getExamHistoryByStuId(stuId);
    }


    @Override
    public List<String> getAllStudentId() {
        return studentRepository.getAllStudentId();
    }

    @Override
    public List<String> getAllStudentRequestId() {
        return studentRepository.getAllStudentRequestId();
    }

    @Override
    public List<Classroom> getAllClassName() {
        return studentRepository.getAllClassName();
    }


    @Override
    public Boolean updateProfileById(StudentUpdateRequest request, String stuId) {
        return studentRepository.updateProfileById(request, stuId);
    }

    @Override
    public Boolean updateStudentIdById(String oldId, String newId) {
        return studentRepository.updateStudentIdById(oldId, newId);
    }

    @Override
    public Boolean changePassword(String id, String newPassword) {
        return studentRepository.changePassword(id, newPassword);
    }

    @Override
    public String getOldPassword(String id) {
        return studentRepository.getOldPassword(id);
    }


    @Override
    public List<Exam> getActiveExams() {
        return studentRepository.getActiveExams();
    }


    @Override
    public int getClassIdByStuId(String stuId) {
        return studentRepository.getClassIdByStuId(stuId);
    }

    @Override
    public List<Result> checkResultBeforeJoinExam(String stuId) {
        return studentRepository.checkResultBeforeJoinExam(stuId);
    }


    @Override
    public ExamPaper getExam() {
        return studentRepository.getExam();
    }


    @Override
    public List<AssignAll> getAssignedExamForStudent(String id) {
        return studentRepository.getAssignedExamForStudent(id);
    }

    @Override
    public List<AssignClass> getAssignedExamForClass(int id) {
        return studentRepository.getAssignedExamForClass(id);
    }


    @Override
    public int getExamStatusById(String id) {
        return studentRepository.getExamStatusById(id);
    }
}
