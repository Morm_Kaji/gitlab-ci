package com.kshrd.hrdexam.service.studentService;

import com.kshrd.hrdexam.model.*;
import com.kshrd.hrdexam.payload.dto.studentDto.ResultDto;
import com.kshrd.hrdexam.payload.dto.studentDto.StudentUpdateRequest;
import com.kshrd.hrdexam.payload.dto.teacherDto.ExamPaper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface StudentService {

    public Boolean registerRequest(StudentRequest request);
    public Classroom getClassIdByClassName(String name);
    Student loadStudentById(String stuId);
    public Student findByEmail(String email);
    public Student findByResetPasswordToken(String token);
    public void updateResetPasswordToken(String token, String email) throws Exception;
    public Student getByResetPasswordToken(String token);
    public void updatePassword(Student student, String newPassword);
    List<ResultDto> getExamHistoryByStuId(String stuId);
    List<String> getAllStudentId();
    List<String> getAllStudentRequestId();
    List<Classroom> getAllClassName();
    Boolean updateProfileById(StudentUpdateRequest request, String stuId);
    Boolean updateStudentIdById(String oldId, String newId);
    Boolean changePassword(String id, String newPassword);
    String getOldPassword(String id);
    List<Exam> getActiveExams();
    int getClassIdByStuId(String stuId);
    List<Result> checkResultBeforeJoinExam(String stuId);
    ExamPaper getExam();
    List<AssignAll> getAssignedExamForStudent(String id);
    List<AssignClass> getAssignedExamForClass(int id);
    int getExamStatusById(String id);
}
