package com.kshrd.hrdexam.service.teacherService;

import com.kshrd.hrdexam.model.*;
import com.kshrd.hrdexam.model.Result;
import com.kshrd.hrdexam.payload.dto.studentDto.StudentResponse;
import com.kshrd.hrdexam.payload.dto.teacherDto.*;
import com.kshrd.hrdexam.payload.request.teacherRequest.*;
import org.apache.ibatis.annotations.*;


import java.util.List;

public interface ExamService {


    Boolean createExam(Exam exam);
    List<Exam> getAllUnassignedExam();
    ExamGenerationDto getAllAssignedExamByGen(int generation);
    Boolean createSection(Section section);
    List<Section> getSectionByExamId(String examId);
    Boolean updateExam(String id, ExamRequest examRequest);
    Boolean updateSection(String id, Section section);
    Boolean deleteExamById(String id);
    Exam getExamById(String id);
    Boolean deleteSectionById(String id);
    Section getSectionById(String id);
    Boolean createQuestion(Question question);
    List<Question> getQuestionBySectionId(String id);
    Question getQuestionById(String id);
    Boolean updateQuestion(String id, QuestionRequest questionRequest);
    Boolean deleteQuestionById(String id);
    Boolean createAnswer(Answer answer);
    GenerationDto getAllStudentsByGeneration();
    List<Student> getStudentIdByGeneration(int id);
    Boolean assignAllStudents(String examId, String stuId);
    List<AssignAll> getIdAssignedStudent();
    List<AssignClass> getAssignedClass();
    Boolean activateExam(String id);
    Boolean deactivateExam(String id);
    Boolean assignToClass(String examId, int classId);
    int getClassIdByStuId(String stuId);
    String getExamIdByStuId(String stuId);
    String getExamIdByClassId(Integer classId);
    Boolean insertResult(String examId,Result result);
    List<ResultResponse> getSubmittedResultsByExamId(String examId);
    Boolean correctAnswerByResultId(String resultId, float score);
    Result viewResultById(String resultId);
    Boolean updateResultToChecking(String resultId);
    List<Result> CheckingResultsByExamId(String examId);
    ExamGenerationDto getAllAdvanceAssignedExamByGen(int generation);
    List<Generation> getAllBasicGeneration();
    List<Generation> getAllAdvanceGeneration();
    List<Integer> getSectionOrderByExamId(String examId);
    ExamPaper getExamPaperById(String id);
    List<SectionResponse> getAllSectionByExamId(String examId);
    List<AssignType> getAllAssignTypes();
    Boolean updateAnswer( UpdateAnswerRequest answerRequest);
    Boolean updateQuestionTypeByQuestionId(String questionId,String newQuestionType);
    Boolean deleteAnswerByQuestionId(String id);
    Exam getExam();
    Boolean updateExamAssignTypeId(String ExamId, int newType);
    List<Exam> getAllAssignedExam();
    Boolean saveResult(SaveResultRequest resultRequest);
    Boolean turnInResult( String ExamId);
}
