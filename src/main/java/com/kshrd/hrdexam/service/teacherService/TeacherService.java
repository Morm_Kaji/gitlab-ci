package com.kshrd.hrdexam.service.teacherService;

import com.kshrd.hrdexam.model.Generation;
import com.kshrd.hrdexam.payload.dto.studentDto.StudentResponse;
import com.kshrd.hrdexam.payload.dto.teacherDto.GenerationDto;
import com.kshrd.hrdexam.model.Student;
import com.kshrd.hrdexam.model.StudentRequest;
import com.kshrd.hrdexam.model.Teacher;
import com.kshrd.hrdexam.payload.request.teacherRequest.ProfileUpdateRequest;
import com.kshrd.hrdexam.payload.request.teacherRequest.UpdateStudentRequest;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface TeacherService {

    List<StudentRequest> getAllStuRequest();
    StudentRequest getReqStuById(String id);
    public Boolean insertAcceptedStu(Student student);
    public int getCurrentGenId();
    Boolean deleteAcceptedStuById(String id);
    Teacher loadTeacherByUsername(String username);
    public Teacher findByEmail(String email);
    public Teacher findByResetPasswordToken(String token);
    public void updateResetPasswordToken(String token, String email) throws Exception;
    public Teacher getByResetPasswordToken(String token);
    public void updatePassword(Teacher teacher, String newPassword);
    GenerationDto getAllStudentsByGeneration(int generation);
    Boolean deleteStudentById(String id);
    Student getStudentById(String id);
    List<StudentResponse> getGenerationIdByGeneration(int id);
    Generation getGenerationNameById(int id);
    Boolean updateStudentById(UpdateStudentRequest request, String stuId);
    Boolean updateProfile(ProfileUpdateRequest request, String username);
    GenerationDto getAllBasicStudentsByGeneration(int generation);
    GenerationDto getAllAdvanceStudentsByGeneration(int generation);
    List<StudentResponse> getAllStudentsByClassId(int id);
    Boolean changePassword(String username, String newPassword);
    String getOldPassword(String username);
    List<String> getAllTeacherUsername();
    List<StudentResponse> getAllStudentsInCurrentGen(int id);
    List<Generation> getAllGenerations();

}
