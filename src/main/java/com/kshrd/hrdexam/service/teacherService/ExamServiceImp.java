package com.kshrd.hrdexam.service.teacherService;


import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kshrd.hrdexam.controller.teacher.TeacherRestController;
import com.kshrd.hrdexam.model.*;
import com.kshrd.hrdexam.payload.dto.teacherDto.*;
import com.kshrd.hrdexam.payload.request.teacherRequest.*;
import com.kshrd.hrdexam.repository.teacherRepository.ExamRepository;
import org.modelmapper.internal.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ExamServiceImp implements ExamService{

    @Autowired
    private ExamRepository examRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public Boolean createExam(Exam exam) {
        return examRepository.createExam(exam);
    }

    @Override
    public List<Exam> getAllUnassignedExam() {
        return examRepository.getAllUnassignedExam();
    }

    @Override
    public ExamGenerationDto getAllAssignedExamByGen(int generation) {
        return examRepository.getAllAssignedExamByGen(generation);
    }

    @Override
    public Boolean createSection(Section section) {
        return examRepository.createSection(section);
    }

    @Override
    public List<Section> getSectionByExamId(String examId) {
        return examRepository.getSectionByExamId(examId);
    }

    @Override
    public Boolean updateExam(String id, ExamRequest examRequest) {
        return examRepository.updateExam(id, examRequest);
    }

    @Override
    public Boolean updateSection(String id, Section section) {
        return examRepository.updateSection(id, section);
    }

    @Override
    public Boolean deleteExamById(String id) {
        return examRepository.deleteExamById(id);
    }

    @Override
    public Exam getExamById(String id) {
        return examRepository.getExamById(id);
    }

    @Override
    public Boolean deleteSectionById(String id) {
        return examRepository.deleteSectionById(id);
    }

    @Override
    public Section getSectionById(String id) {
        return examRepository.getSectionById(id);
    }

    @Override
    public Boolean createQuestion(Question question) {
        return examRepository.createQuestion(question);
    }

    @Override
    public List<Question> getQuestionBySectionId(String id) {
        return examRepository.getQuestionBySectionId(id);
    }

    @Override
    public Question getQuestionById(String id) {
        return examRepository.getQuestionById(id);
    }

    @Override
    public Boolean updateQuestion(String id, QuestionRequest questionRequest) {
        return examRepository.updateQuestion(id, questionRequest);
    }

    @Override
    public Boolean deleteQuestionById(String id) {
        return examRepository.deleteQuestionById(id);
    }

    @Override
    public Boolean createAnswer(Answer answer) {

        String questionTypeId = examRepository.getQuestionTypeIdByQuestionId(answer.getQuestionId());
        answer.setId(RandomString.make(20));

        try {
            if (questionTypeId.equals("2")){

                List<FillInGape> fillInGapeList = objectMapper.readValue(answer.getAnswer().toString() , new TypeReference<>() {
                });


                Map<String, Object> answerDetail = new HashMap<>();
                answerDetail.put("answer", fillInGapeList);

                answer.setAnswer(answerDetail);

            }else if (questionTypeId.equals("3")){

                List<MultipleChoice> multipleChoiceList = objectMapper.readValue(answer.getAnswer().toString() , new TypeReference<>() {
                });


                Map<String, Object> answerDetail = new HashMap<>();
                answerDetail.put("answer", multipleChoiceList);

                answer.setAnswer(answerDetail);

            }else {
                ConvertJsonAnswer jsonAnswer = objectMapper.readValue(answer.getAnswer().toString(), ConvertJsonAnswer.class);


                Map<String, Object> answerDetail = new HashMap<>();
                answerDetail.put("answer", jsonAnswer);

                answer.setAnswer(answerDetail);
            }


        }catch (JsonProcessingException e){
            e.printStackTrace();
        }

        return examRepository.createAnswer(answer);
    }

    @Override
    public GenerationDto getAllStudentsByGeneration() {
        return examRepository.getAllStudentsByGeneration();
    }

    @Override
    public List<Student> getStudentIdByGeneration(int id) {
        return examRepository.getStudentIdByGeneration(id);
    }

    @Override
    public Boolean assignAllStudents(String examId, String stuId) {
        return examRepository.assignAllStudents(examId, stuId);
    }

    @Override
    public List<AssignAll> getIdAssignedStudent() {
        return examRepository.getIdAssignedStudent();
    }

    @Override
    public List<AssignClass> getAssignedClass() {
        return examRepository.getAssignedClass();
    }

    @Override
    public Boolean activateExam(String id) {
        return examRepository.activateExam(id);
    }

    @Override
    public Boolean deactivateExam(String id) {
        return examRepository.deactivateExam(id);
    }

    @Override
    public Boolean assignToClass(String examId, int classId) {
        return examRepository.assignToClass(examId, classId);
    }

    @Override
    public int getClassIdByStuId(String stuId) {
        return examRepository.getClassIdByStuId(stuId);
    }

    @Override
    public String getExamIdByStuId(String stuId) {
        return examRepository.getExamIdByStuId(stuId);
    }

    @Override
    public String getExamIdByClassId(Integer classId) {
        return examRepository.getExamIdByClassId(classId);
    }

    @Override
    public Boolean insertResult(String examId,Result result) {

        return examRepository.insertResult(examId, result);
    }

    @Override
    public List<ResultResponse> getSubmittedResultsByExamId(String examId) {
        return examRepository.getSubmittedResultsByExamId(examId);
    }

    @Override
    public Boolean correctAnswerByResultId(String resultId, float score) {
        return examRepository.correctAnswerByResultId(resultId, score);
    }

    @Override
    public Result viewResultById(String resultId) {
        return examRepository.viewResultById(resultId);
    }

    @Override
    public Boolean updateResultToChecking(String resultId) {
        return examRepository.updateResultToChecking(resultId);
    }

    @Override
    public List<Result> CheckingResultsByExamId(String examId) {
        return examRepository.CheckingResultsByExamId(examId);
    }

    @Override
    public ExamGenerationDto getAllAdvanceAssignedExamByGen(int generation) {
        return examRepository.getAllAdvanceAssignedExamByGen(generation);
    }


    @Override
    public List<Generation> getAllBasicGeneration() {
        return examRepository.getAllBasicGeneration();
    }

    @Override
    public List<Generation> getAllAdvanceGeneration() {
        return examRepository.getAllAdvanceGeneration();
    }


    @Override
    public List<Integer> getSectionOrderByExamId(String examId) {
        return examRepository.getSectionOrderByExamId(examId);
    }

    @Override
    public ExamPaper getExamPaperById(String id) {
        return examRepository.getExamPaperById(id);
    }

    @Override
    public List<SectionResponse> getAllSectionByExamId(String examId) {
        return examRepository.getAllSectionByExamId(examId);
    }

    @Override
    public List<AssignType> getAllAssignTypes() {
        return examRepository.getAllAssignTypes();
    }


    @Override
    public Boolean updateAnswer(UpdateAnswerRequest answerRequest){

        String questionId = examRepository.getQuestionIdByAnswerId(answerRequest.getAnswerId());
        String questionTypeId = examRepository.getQuestionTypeIdByQuestionId(questionId);


        Map<String, Object> answerDetail = new HashMap<>();

        objectMapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);

        try{

            if (questionTypeId.equals("2")){

                List<FillInGape> fillInGapeList = objectMapper.readValue(answerRequest.getAnswer().toString() , new TypeReference<>() {
                });


                answerDetail.put("answer", fillInGapeList);
                answerRequest.setAnswerId(answerRequest.getAnswerId());
                answerRequest.setAnswer(answerDetail);

            }else if (questionTypeId.equals("3")){

                List<MultipleChoice> multipleChoiceList = objectMapper.readValue(answerRequest.getAnswer().toString() , new TypeReference<>() {
                });


                answerDetail.put("answer", multipleChoiceList);
                answerRequest.setAnswerId(answerRequest.getAnswerId());
                answerRequest.setAnswer(answerDetail);

            }else {

                ConvertJsonAnswer jsonAnswer = objectMapper.readValue(answerRequest.getAnswer().toString(), ConvertJsonAnswer.class);

                answerDetail.put("answer", jsonAnswer);

                answerRequest.setAnswerId(answerRequest.getAnswerId());
                answerRequest.setAnswer(answerDetail);

            }

        }catch (JsonProcessingException e){
            e.printStackTrace();
        }



        return examRepository.updateAnswer(answerRequest);
    }

    @Override
    public Boolean updateQuestionTypeByQuestionId(String questionId, String newQuestionType) {
        return examRepository.updateQuestionTypeByQuestionId(questionId, newQuestionType);
    }

    @Override
    public Boolean deleteAnswerByQuestionId(String id) {
        return examRepository.deleteAnswerByQuestionId(id);
    }

    @Override
    public Exam getExam() {
        return examRepository.getExam();
    }

    @Override
    public Boolean updateExamAssignTypeId(String ExamId, int newType) {
        return examRepository.updateExamAssignTypeId(ExamId, newType);
    }

    @Override
    public List<Exam> getAllAssignedExam() {
        return examRepository.getAllAssignedExam();
    }

    @Override
    public Boolean saveResult(SaveResultRequest resultRequest) {

        Map<String, Object> result = new HashMap<>();

        objectMapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);

        try{

            ExamPaper examPaper = objectMapper.readValue(resultRequest.getResult().toString(), ExamPaper.class);

            result.put("results", examPaper);
            resultRequest.setResultId(resultRequest.getResultId());
            resultRequest.setResult(result);
            resultRequest.setStatus(resultRequest.getStatus());
            resultRequest.setScore(resultRequest.getScore());


        }catch (JsonProcessingException e){
            e.printStackTrace();
        }


        return examRepository.saveResult(resultRequest);
    }


    @Override
    public Boolean turnInResult(String ExamId) {
        return examRepository.turnInResult(ExamId);
    }
}
