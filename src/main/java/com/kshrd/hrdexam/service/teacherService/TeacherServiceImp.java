package com.kshrd.hrdexam.service.teacherService;

import com.kshrd.hrdexam.model.Generation;
import com.kshrd.hrdexam.payload.dto.studentDto.StudentResponse;
import com.kshrd.hrdexam.payload.dto.teacherDto.GenerationDto;
import com.kshrd.hrdexam.model.Student;
import com.kshrd.hrdexam.model.StudentRequest;
import com.kshrd.hrdexam.model.Teacher;
import com.kshrd.hrdexam.payload.request.teacherRequest.ProfileUpdateRequest;
import com.kshrd.hrdexam.payload.request.teacherRequest.UpdateStudentRequest;
import com.kshrd.hrdexam.repository.teacherRepository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeacherServiceImp implements TeacherService{

    @Autowired
    private TeacherRepository teacherRepository;

    @Override
    public List<StudentRequest> getAllStuRequest() {
        return teacherRepository.getAllStudentsRequest();
    }

    @Override
    public StudentRequest getReqStuById(String id) {
        return teacherRepository.getReqStuById(id);
    }

    @Override
    public Boolean insertAcceptedStu(Student student) {
        return teacherRepository.insertAcceptedStu(student);
    }

    @Override
    public int getCurrentGenId() {
        return teacherRepository.getCurrentGenId();
    }

    @Override
    public Boolean deleteAcceptedStuById(String id) {
        return teacherRepository.deleteAcceptedStuById(id);
    }

    @Override
    public Teacher loadTeacherByUsername(String username) {
        return teacherRepository.loadTeacherByUsername(username);
    }

    @Override
    public Teacher findByEmail(String email) {
        return teacherRepository.findByEmail(email);
    }

    @Override
    public Teacher findByResetPasswordToken(String token) {
        return teacherRepository.findByResetPasswordToken(token);
    }




    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @Override
    public void updateResetPasswordToken(String token, String email) throws Exception {
        Teacher teacher = teacherRepository.findByEmail(email);
        if (teacher != null) {
            teacher.setResetPasswordToken(token);
            teacherRepository.save(teacher, teacher.getId());
        } else {
            throw new Exception("Could not find any user with the email " + email);
        }
    }

    @Override
    public Teacher getByResetPasswordToken(String token) {
        return teacherRepository.findByResetPasswordToken(token);
    }




    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @Override
    public void updatePassword(Teacher teacher, String newPassword) {

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode(newPassword);
        teacher.setPassword(encodedPassword);

        teacher.setResetPasswordToken(null);
        teacherRepository.save(teacher, teacher.getId());
    }



    @Override
    public GenerationDto getAllStudentsByGeneration(int generation) {
        return teacherRepository.getAllStudentsByGeneration(generation);
    }



    @Override
    public Boolean deleteStudentById(String id) {
        return teacherRepository.deleteStudentById(id);
    }

    @Override
    public Student getStudentById(String id) {
        return teacherRepository.getStudentById(id);
    }

    @Override
    public List<StudentResponse> getGenerationIdByGeneration(int id) {
        return teacherRepository.getGenerationIdByGeneration(id);
    }

    @Override
    public Generation getGenerationNameById(int id) {
        return teacherRepository.getGenerationNameById(id);
    }

    @Override
    public Boolean updateStudentById(UpdateStudentRequest request, String stuId) {
        return teacherRepository.updateStudentById(request, stuId);
    }

    @Override
    public Boolean updateProfile(ProfileUpdateRequest request, String username) {
        return teacherRepository.updateProfile(request, username);
    }

    @Override
    public GenerationDto getAllBasicStudentsByGeneration(int generation) {
        return teacherRepository.getAllBasicStudentsByGeneration(generation);
    }

    @Override
    public GenerationDto getAllAdvanceStudentsByGeneration(int generation) {
        return teacherRepository.getAllAdvanceStudentsByGeneration(generation);
    }

    @Override
    public List<StudentResponse> getAllStudentsByClassId(int id) {
        return teacherRepository.getAllStudentsByClassId(id);
    }

    @Override
    public Boolean changePassword(String username, String newPassword) {
        return teacherRepository.changePassword(username, newPassword);
    }

    @Override
    public String getOldPassword(String username) {
        return teacherRepository.getOldPassword(username);
    }

    @Override
    public List<String> getAllTeacherUsername() {
        return teacherRepository.getAllTeacherUsername();
    }

    @Override
    public List<StudentResponse> getAllStudentsInCurrentGen(int id) {
        return teacherRepository.getGenerationIdByGeneration(id);
    }

    @Override
    public List<Generation> getAllGenerations() {
        return teacherRepository.getAllGenerations();
    }
}
