FROM openjdk:14
WORKDIR /hrdexam-api/
COPY /target/hrdexam.jar /hrdexam-api/
EXPOSE 8081
ENTRYPOINT [ "java","-Djava.security.egd=file:/dev/./urandom", "-jar", "/hrdexam-api/hrdexam.jar" ]