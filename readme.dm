

	--->>>>>>> All Api in HRD Exam 9th <<<<<<<-----

	URL =>>> http://localhost:8080/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config#

	+ StudentRestController
		this controller has all students actions such as :
	- registerStudent : allow student register account.
		model : {
  			    "id": "PP0001",
  			    "fullName": "Nou Reach",
  			    "email": "noureach77@gmail.com",
  			    "password": "noureach",
  			    "className": "Phnom Penh",
  			    "gender": "Male"
			}
		* Student's ID can not be duplicate.

	- loginStudent
		model : {
  			    "id": "PP0001",
  			    "password": "noureach"
			}

		demo login : username : 270298, password : 270198

		* Student's ID and password must be at least 6 characters.

	- getAssignedExamByStuId
	- getAssignedExamByClassId
	- getExamHistoryByStuId
	- submit
	- getAllClassName
	- updateProfileById
	- changePassword
	- getProfile
	- joinExam

	+ TeacherRestController : this controller have all students actions such as

	- loginTeacher
		model : {
  			    "username": "noureach",
  			    "password": "123456"
			}

		demo login : username : reach, password : 123456

		* Teacher's username are only character and don't have space are allowed.
		* Password must be at least 6 chracters

	- getAllStudentRequest
	- acceptStudentRequestById
	- getAllStudentsByGeneration
	- getAllBasicStudentsByGeneration
	- getAllAdvanceStudentsByGeneration
	- deleteStudentById
	- updateStudentById
	- updateProfile
	- rejectStudentById
	- getResultsByExamId
	- correctAnswerByResultId
	- viewResultById
	- getCheckingResultByExamId
	- getAllBasicGeneration
	- getAllAdvanceGeneration
	- changePassword
	- getProfile
	- getAllStudentsInCurrentGeneration


	+ ExamRestController
		this controller allow teacher manage on examination such as :

	- createExam
		model : {
  			    "examName": "Monthly Exam",
  			    "examSubject": "Java",
  			    "examDate": "20-08-2021 09:00",
  			    "totalScore": 60,
 			    "duration": 60
			}

	- getAllUnassignedExam
	- getAllBasicAssignedExamByGen
	- getAllAdvanceAssignedExamByGen
	- createSection
		model : {
  			    "section": 1,   //section order
  			    "heading": "Choose the correct answer.",
  			    "examId": "WXlYZN5fsckrAFjSTB4p"  //Rendom string ID
			}
		* examId is from selected exam that exist in database

	- getSectionByExamId
	- updateExamById
	- updateSection
	- deleteExamById
	- deleteSectionById
	- createQuestion
		model : {
  			    "sectionId": "7ZACDSJC0dWUX5AVVUAR", //Rendom string ID
  		 	    "questionTypeId": "1",
  			    "questionContent": "What is Java?",
  			    "image": "null",
  			    "score": 3
			}
		* sectionId is from selected section that exist in database

	- getQuestionBySectionId
	- deleteQuestionById
	- updateQuestions
	- createAnswer
		model : {
  			    "questionId": "dUeXGBCW08el0D1LFEGp", //Rendom string ID
  			    "answer": {"answer" : "Java is....."} // Json
			}
		* questionId is from selected question that exist in database

	- assignAllStudents
	- assignSpecificStudent
	- assignSpecificClass
	- deactivateExam
	- getExamById



	+ FileUploadRestController
		this controller allows user upload profile image.

	- uploadFile
	- getAllFile
	- getFileById

	+ ForgotPassword
		this allow HRD Exam send user a reset password link to reset new password.

	- teacherProcessForgotPassword
	- studentProcessForgotPassword
	- sendEmail


	+ ResetPassword
		this controller allow user reset new password.

	- teacherShowResetPasswordForm
	- teacherProcessResetPassword
	- studentShowResetPasswordForm
	- studentProcessResetPassword

	+ Utility : this class is use for generate reset password URL.

	+ Success Rest Controller 100%

	- FileUpdoadRestConttroller
	- ForgotPassword
	- ResetPassword

	+ Security implemented.
	+ Have problem Rest Controller

	- StudentRestController
		- join exam
		- submit
	- TeacherRestController
		- Auto Correct
	- ExamRestController
		- Assign








